#  Copyright (C) 2021,2022 Mehmet Direnc Mungan
#  University of Tuebingen
#  Interfaculty Institute of Microbiology and Infection Medicine
#  Lab of Nadine Ziemert, Div. of Microbiology/Biotechnology
#  Funding by the German Centre for Infection Research (DZIF)
#  #
#  This file is part of SeMa-Trap
#  SeMa-Trap is free software. you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  #
#  #This program is distributed in the hope that it will be useful,
#   #but WITHOUT ANY WARRANTY; without even the implied warranty of
#   #MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   #GNU General Public License for more details.
#
#  License: You should have received a copy of the GNU General Public License v3 with SeMa-Trap
#  A copy of the GPLv3 can also be found at: <http://www.gnu.org/licenses/>.

import os
from pathlib import Path
import pickle,json
import logging


def get_regulated_genes(working_dir_list, fc_cutoff_up, fc_cutoff_down):
    up_dict = {}
    down_dict = {}
    same_dict = {}
    for working_dir in working_dir_list:
        working_dir_base = os.path.basename(Path(working_dir))
        with open(os.path.join(working_dir, "master_dict.json")) as f:
            master_dict = json.load(f)
        for gene in master_dict:
            if "padj" in master_dict[gene]:
                padj = master_dict[gene]["padj"]
            else:
                continue
            if padj != "NA":
                if float(padj) > 0.05:
                    continue
                else:
                    fc = master_dict[gene]["fold_change"]
                    if float(fc) >= fc_cutoff_up:
                        if working_dir_base not in up_dict:
                            up_dict[working_dir_base] = {gene : [float(fc)]}
                        else:
                            if gene not in up_dict[working_dir_base]:
                                up_dict[working_dir_base][gene] = [float(fc)]
                            else:
                                up_dict[working_dir_base][gene].append(float(fc))
                    elif float(fc) <= fc_cutoff_down:
                        if working_dir_base not in down_dict:
                            down_dict[working_dir_base] = {gene : [float(fc)]}
                        else:
                            if gene not in down_dict[working_dir_base]:
                                down_dict[working_dir_base][gene] = [float(fc)]
                            else:
                                down_dict[working_dir_base][gene].append(float(fc))
                    else:
                        if working_dir_base not in same_dict:
                            same_dict[working_dir_base] = {gene : [float(fc)]}
                        else:
                            if gene not in same_dict[working_dir_base]:
                                same_dict[working_dir_base][gene] = [float(fc)]
                            else:
                                same_dict[working_dir_base][gene].append(float(fc))
            else:
                continue
    return up_dict, down_dict, same_dict




def get_regulated_bgcs(working_dir_list, fc_cutoff_up, fc_cutoff_down):
    bgc_up = {}
    bgc_down = {}
    bgc_same = {}
    for working_dir in working_dir_list:
        working_dir_base = os.path.basename(Path(working_dir))
        with open(os.path.join(working_dir, "bgcs.pickle"), "rb") as bg:
            bgcs = pickle.load(bg)
        for bgc in bgcs:
            fc = bgcs[bgc]["stats"]["fold_change_biosynthetic"]
            if type(fc) == list:
                fc = 0.0
            else:
                fc = float(fc)
            if float(fc) >= fc_cutoff_up:
                if working_dir_base not in bgc_up:
                    bgc_up[working_dir_base] = {bgc: [float(fc)]}
                else:
                    if bgc not in bgc_up[working_dir_base]:
                        bgc_up[working_dir_base][bgc] = [float(fc)]
                    else:
                        bgc_up[working_dir_base][bgc].append(float(fc))
            elif float(fc) <= fc_cutoff_down:
                if working_dir_base not in bgc_down:
                    bgc_down[working_dir_base] = {bgc: [float(fc)]}
                else:
                    if bgc not in bgc_down[working_dir_base]:
                        bgc_down[working_dir_base][bgc] = [float(fc)]
                    else:
                        bgc_down[working_dir_base][bgc].append(float(fc))
            else:
                if working_dir_base not in bgc_same:
                    bgc_same[working_dir_base] = {bgc: [float(fc)]}
                else:
                    if bgc not in bgc_same[working_dir_base]:
                        bgc_same[working_dir_base][bgc] = [float(fc)]
                    else:
                        bgc_same[working_dir_base][bgc].append(float(fc))
    return bgc_up, bgc_down, bgc_same


def get_description(master_dict, gene):
    if master_dict[gene]["annotations"] != [] and master_dict[gene]["annotations"] != "None" and master_dict[gene]["annotations"] != "N/A":
        description = master_dict[gene]["annotations"]
    elif master_dict[gene]["product"] != "hypothetical protein":
        description = ["product||" + master_dict[gene]["product"]]
    elif master_dict[gene]["resistance"] != "N/A":
        description = ["resistance||" + master_dict[gene]["resistance"]]
    elif master_dict[gene]["regulator"] != "N/A":
        description = ["regulator||" + master_dict[gene]["regulator"]]
    else:
        description = ["product||" + master_dict[gene]["product"]]
    return description



def comparison(working_dir_list, fc_cutoff_up, fc_cutoff_down):
    condition_num = len(working_dir_list)
    parent_path = Path(working_dir_list[0]).parent.absolute()
    with open(os.path.join(working_dir_list[0], "bgcs.pickle"), "rb") as bg:
        bgcs = pickle.load(bg)
    with open(os.path.join(working_dir_list[0], "master_dict.json")) as f:
        master_dict = json.load(f)
    bgc_genes = {}
    for i in master_dict:
        if "bgc_prox" in master_dict[i] and master_dict[i]["bgc_prox"] == "true":
            if master_dict[i]["known_cluster"] != "N/A":
                first_bgc = master_dict[i]["known_cluster"]
                if first_bgc not in bgc_genes:
                    bgc_genes[first_bgc] = [i]
                else:
                    bgc_genes[first_bgc].append(i)
            else:
                first_bgc = master_dict[i]["region_number"]
                if first_bgc not in bgc_genes:
                    bgc_genes[first_bgc] = [i]
                else:
                    bgc_genes[first_bgc].append(i)
            # bgc_genes.append(i)
    genes_up_dict, genes_down_dict, genes_same_dict = get_regulated_genes(working_dir_list, fc_cutoff_up, fc_cutoff_down)
    bgcs_up_dict, bgcs_down_dict, bgcs_same_dict = get_regulated_bgcs(working_dir_list, fc_cutoff_up, fc_cutoff_down)
    concordant = {}
    reverse = {}
    for working_dir in working_dir_list:
        working_dir_base = os.path.basename(Path(working_dir))
        if working_dir_base in bgcs_up_dict and working_dir_base in genes_up_dict:
            for bgc in bgcs_up_dict[working_dir_base]:
                for gene in genes_up_dict[working_dir_base]:
                    if bgc in bgc_genes:
                        if gene in bgc_genes[bgc] or "product" not in master_dict[gene]:
                            continue
                    comb = bgc + "||" + gene
                    if comb not in concordant:
                        description = get_description(master_dict, gene)
                        concordant[comb] = {"bgc" : bgc, "gene" : gene, "total": abs(float(bgcs_up_dict[working_dir_base][bgc][0]) * float(genes_up_dict[working_dir_base][gene][0])),
                                            "count" : 1.0 , "description" : description, "housekeeping": master_dict[gene]["housekeeping"],
                                            working_dir_base : [float(bgcs_up_dict[working_dir_base][bgc][0]), float(genes_up_dict[working_dir_base][gene][0])]}
                        # if condition_num == 1:
                        #     concordant[comb]["total"] = abs(float(bgcs_up_dict[working_dir_base][bgc][0]) * float(genes_up_dict[working_dir_base][gene][0]))
                    else:
                        concordant[comb]["total"] += abs(float(bgcs_up_dict[working_dir_base][bgc][0]) * float(genes_up_dict[working_dir_base][gene][0]))
                        concordant[comb][working_dir_base] = [float(bgcs_up_dict[working_dir_base][bgc][0]), float(genes_up_dict[working_dir_base][gene][0])]
                        concordant[comb]["count"] += 1
        if working_dir_base in bgcs_down_dict and working_dir_base in genes_down_dict:
            for bgc in bgcs_down_dict[working_dir_base]:
                for gene in genes_down_dict[working_dir_base]:
                    if bgc in bgc_genes:
                        if gene in bgc_genes[bgc] or "product" not in master_dict[gene]:
                            continue
                    comb = bgc + "||" + gene
                    if comb not in concordant:
                        description = get_description(master_dict, gene)
                        concordant[comb] = {"bgc" : bgc, "gene" : gene, "total": abs(float(bgcs_down_dict[working_dir_base][bgc][0]) * float(genes_down_dict[working_dir_base][gene][0])),
                                            "count" : 1.0 , "description" : description, "housekeeping": master_dict[gene]["housekeeping"],
                                            working_dir_base : [float(bgcs_down_dict[working_dir_base][bgc][0]), float(genes_down_dict[working_dir_base][gene][0])]}
                        # if condition_num == 1:
                        #     concordant[comb]["total"] = abs(float(bgcs_down_dict[working_dir_base][bgc][0]) * float(genes_down_dict[working_dir_base][gene][0]))
                    else:
                        concordant[comb]["total"] += abs(float(bgcs_down_dict[working_dir_base][bgc][0]) * float(genes_down_dict[working_dir_base][gene][0]))
                        concordant[comb][working_dir_base] = [float(bgcs_down_dict[working_dir_base][bgc][0]), float(genes_down_dict[working_dir_base][gene][0])]
                        concordant[comb]["count"] += 1
        if working_dir_base in bgcs_up_dict and working_dir_base in genes_down_dict:
            for bgc in bgcs_up_dict[working_dir_base]:
                for gene in genes_down_dict[working_dir_base]:
                    if bgc in bgc_genes:
                        if gene in bgc_genes[bgc] or "product" not in master_dict[gene]:
                            continue
                    comb = bgc + "||" + gene
                    if comb not in reverse:
                        description = get_description(master_dict, gene)
                        reverse[comb] = {"bgc" : bgc, "gene" : gene, "total": abs(float(bgcs_up_dict[working_dir_base][bgc][0]) * float(genes_down_dict[working_dir_base][gene][0])),
                                         "count" : 1.0 , "description" : description, "housekeeping": master_dict[gene]["housekeeping"],
                                            working_dir_base : [float(bgcs_up_dict[working_dir_base][bgc][0]), float(genes_down_dict[working_dir_base][gene][0])]}
                        # if condition_num == 1:
                        #     reverse[comb]["total"] = abs(float(bgcs_up_dict[working_dir_base][bgc][0]) * float(genes_down_dict[working_dir_base][gene][0]))
                    else:
                        reverse[comb]["total"] += abs(float(bgcs_up_dict[working_dir_base][bgc][0]) * float(genes_down_dict[working_dir_base][gene][0]))
                        reverse[comb][working_dir_base] = [float(bgcs_up_dict[working_dir_base][bgc][0]), float(genes_down_dict[working_dir_base][gene][0])]
                        reverse[comb]["count"] += 1
        if working_dir_base in bgcs_down_dict and working_dir_base in genes_up_dict:
            for bgc in bgcs_down_dict[working_dir_base]:
                for gene in genes_up_dict[working_dir_base]:
                    if bgc in bgc_genes:
                        if gene in bgc_genes[bgc] or "product" not in master_dict[gene]:
                            continue
                    comb = bgc + "||" + gene
                    if comb not in reverse:
                        description = get_description(master_dict, gene)
                        reverse[comb] = {"bgc" : bgc, "gene" : gene, "total": abs(float(bgcs_down_dict[working_dir_base][bgc][0]) * float(genes_up_dict[working_dir_base][gene][0]))
                            , "count" : 1.0 , "description" : description, "housekeeping": master_dict[gene]["housekeeping"],
                                            working_dir_base : [float(bgcs_down_dict[working_dir_base][bgc][0]), float(genes_up_dict[working_dir_base][gene][0])]}
                    else:
                        reverse[comb]["total"] += abs(float(bgcs_down_dict[working_dir_base][bgc][0]) * float(genes_up_dict[working_dir_base][gene][0]))
                        reverse[comb][working_dir_base] = [float(bgcs_down_dict[working_dir_base][bgc][0]), float(genes_up_dict[working_dir_base][gene][0])]
                        reverse[comb]["count"] += 1
    concordant = dict(sorted(concordant.items(), key=lambda item: (item[1]["count"], item[1]["total"]), reverse = True))
    reverse = dict(sorted(reverse.items(), key=lambda item: (item[1]["count"], item[1]["total"]), reverse = True))
    combined_result_path = os.path.join(parent_path, "combined")
    combined_result_path_all = os.path.join(parent_path, "combined", "all")
    if os.path.exists(combined_result_path) != True:
        os.mkdir(combined_result_path)
    if os.path.exists(combined_result_path_all) != True:
        os.mkdir(combined_result_path_all)
    separate_functions(master_dict, reverse, concordant, combined_result_path)
    single_conc_path = ""
    single_rev_path = ""
    if len(working_dir_list) == 1:
        single_conc_path = os.path.join(working_dir_list[0], "concordant.json")
        with open(single_conc_path, "w") as j:
            json.dump(concordant, j, indent=4)
    else:
        with open(os.path.join(combined_result_path_all, "concordant_all.json"), "w") as j:
            json.dump(concordant, j, indent=4)
    if len(working_dir_list) == 1:
        single_rev_path = os.path.join(working_dir_list[0], "reverse.json")
        with open(single_rev_path, "w") as j:
            json.dump(reverse, j, indent=4)
    else:
        with open(os.path.join(combined_result_path_all, "reverse_all.json"), "w") as j:
            json.dump(reverse, j, indent=4)
    return single_conc_path, single_rev_path

def separate_functions(master_dict, reverse, concordant, combined_result_path):
    transporters_concordant = {}
    regulators_concordant = {}
    resistance_concordant = {}
    secondary_pathway_concordant = {}
    transporters_reverse = {}
    regulators_reverse = {}
    resistance_reverse = {}
    secondary_pathway_reverse = {}
    reverse_results_path = os.path.join(combined_result_path, "reverse_functions")
    concordant_results_path = os.path.join(combined_result_path, "concordant_functions")

    if os.path.exists(reverse_results_path) != True:
        os.mkdir(reverse_results_path)
    if os.path.exists(concordant_results_path) != True:
        os.mkdir(concordant_results_path)

    for gene in reverse:
        if master_dict[gene.split("||")[1]]["secondary"] == "true":
            secondary_pathway_reverse[gene] = reverse[gene]
        for desc in reverse[gene]["description"]:
            if "resistance" in desc.lower() and "human diseases" not in desc.lower():
                if gene not in resistance_reverse:
                    resistance_reverse[gene] = reverse[gene]
            if "transport" in desc.lower() or "export" in desc.lower() or "secret" in desc.lower():
                if gene not in transporters_reverse:
                    transporters_reverse[gene] = reverse[gene]
            if "regulator" in desc.lower() or "transcription factor" in desc.lower() or "helix-turn-helix" in desc.lower() or "repressor" in desc.lower() or "activator" in desc.lower() or "transcriptional" in desc.lower() or "transcription machinery" in desc.lower():
                if gene not in regulators_reverse:
                    regulators_reverse[gene] = reverse[gene]
    for gene in concordant:
        if master_dict[gene.split("||")[1]]["secondary"] == "true":
            secondary_pathway_concordant[gene] = concordant[gene]
        for desc in concordant[gene]["description"]:
            if "resistance" in desc.lower() and "human diseases" not in desc.lower():
                if gene not in resistance_concordant:
                    resistance_concordant[gene] = concordant[gene]
            if "transport" in desc.lower() or "export" in desc.lower() or "secret" in desc.lower():
                if gene not in transporters_concordant:
                    transporters_concordant[gene] = concordant[gene]
            if "regulator" in desc.lower() or "transcription factor" in desc.lower() or "helix-turn-helix" in desc.lower() or "transcriptional" in desc.lower() or "transcription machinery" in desc.lower():
                if gene not in regulators_concordant:
                    regulators_concordant[gene] = concordant[gene]

    with open(os.path.join(concordant_results_path, "secondary_pathway_concordant.json"), "w") as j:
        json.dump(secondary_pathway_concordant, j, indent=4)
    with open(os.path.join(concordant_results_path, "resistance_concordant.json"), "w") as j:
        json.dump(resistance_concordant, j, indent=4)
    with open(os.path.join(concordant_results_path, "transporters_concordant.json"), "w") as j:
        json.dump(transporters_concordant, j, indent=4)
    with open(os.path.join(concordant_results_path, "regulators_concordant.json"), "w") as j:
        json.dump(regulators_concordant, j, indent=4)

    with open(os.path.join(reverse_results_path, "secondary_pathway_reverse.json"), "w") as j:
        json.dump(secondary_pathway_reverse, j, indent=4)
    with open(os.path.join(reverse_results_path, "resistance_reverse.json"), "w") as j:
        json.dump(resistance_reverse, j, indent=4)
    with open(os.path.join(reverse_results_path, "transporters_reverse.json"), "w") as j:
        json.dump(transporters_reverse, j, indent=4)
    with open(os.path.join(reverse_results_path, "regulators_reverse.json"), "w") as j:
        json.dump(regulators_reverse, j, indent=4)



if __name__ == '__main__':
    print("main")