#  Copyright (C) 2021,2022 Mehmet Direnc Mungan
#  University of Tuebingen
#  Interfaculty Institute of Microbiology and Infection Medicine
#  Lab of Nadine Ziemert, Div. of Microbiology/Biotechnology
#  Funding by the German Centre for Infection Research (DZIF)
#  #
#  This file is part of SeMa-Trap
#  SeMa-Trap is free software. you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  #
#  #This program is distributed in the hope that it will be useful,
#   #but WITHOUT ANY WARRANTY; without even the implied warranty of
#   #MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   #GNU General Public License for more details.
#
#  License: You should have received a copy of the GNU General Public License v3 with SeMa-Trap
#  A copy of the GPLv3 can also be found at: <http://www.gnu.org/licenses/>.

import os
import json
import pickle
from multiprocessing import Pool as ThreadPool



def gene_ids(gtf_file):
    gtf = open(gtf_file, "r")
    gene_id_dict = {}
    for line in gtf:
        if line.startswith("#"):
            continue
        if line.split("\t")[2] == "gene":
            GeneID = line.split("GeneID:")[1].split("\"")[0]
            gene_id = line.split("gene_id")[1].split("\"")[1]
            gene_id_dict[gene_id] = GeneID
    print(gene_id_dict)
    return gene_id_dict


def parse_keg_file():
    gene_to_pathway_dict = {}
    keg_file = open("static/ko00001.keg", "r")
    for line in keg_file:
        if line.startswith("A09190"):
            print("asd")
        if line.startswith("A"):
            current_a_path = ""
            metabolism = line[1:].strip()
            a_met_num = metabolism.split()[0]
            a_met_name = " ".join(metabolism.split()[1:])
            current_a_path += a_met_num
            current_a_path += "|-|"
            current_a_path += a_met_name
        if line.startswith("B") and line != "B\n":
            b_met_num = line.split()[1]
            b_met_name = " ".join(line.split()[2:])
            current_b_path = b_met_num + "|-|" + b_met_name + "||" + current_a_path
        if line.startswith("C"):
            split_line = line.split()
            if ":" not in line and current_a_path != '09190|-|Not Included in Pathway or Brite':
                current_c_path = ""
                continue
            if current_a_path =='09190|-|Not Included in Pathway or Brite':
                c_path_num = split_line[1]
                c_path_name = " ".join(split_line[2:])
            else:
                c_path_num = split_line[-1].split(":")[1][0:-1]
                c_path_name = " ".join(split_line[2:-1])
            current_c_path = c_path_num + "|-|" + c_path_name + "||" + current_b_path
        if line.startswith("D"):
            split_line = line.split()
            gene_num = split_line[1]
            if "[EC:" in line:
                gene_name = line.split("; ")[1].split("[EC:")[0].strip()
            else:
                # print(line)
                if ";" not in line:
                    gene_name = "CCAAT/enhancer binding protein (C/EBP)"
                else:
                    gene_name = line.split("; ")[1].strip()
            current_d_path = gene_num + "|-|" + gene_name + "||" + current_c_path
            if current_d_path in list(gene_to_pathway_dict.values()):
               print(current_d_path)
            if gene_num not in gene_to_pathway_dict:
                gene_to_pathway_dict[gene_num] = [current_d_path]
            else:
                gene_to_pathway_dict[gene_num].append(current_d_path)
    with open("static/keg.pickle", "wb") as keg_file:
       pickle.dump(gene_to_pathway_dict, keg_file)



if __name__ == '__main__':
    print("main")