#  Copyright (C) 2021,2022 Mehmet Direnc Mungan
#  University of Tuebingen
#  Interfaculty Institute of Microbiology and Infection Medicine
#  Lab of Nadine Ziemert, Div. of Microbiology/Biotechnology
#  Funding by the German Centre for Infection Research (DZIF)
#  #
#  This file is part of SeMa-Trap
#  SeMa-Trap is free software. you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  #
#  #This program is distributed in the hope that it will be useful,
#   #but WITHOUT ANY WARRANTY; without even the implied warranty of
#   #MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   #GNU General Public License for more details.
#
#  License: You should have received a copy of the GNU General Public License v3 with SeMa-Trap
#  A copy of the GPLv3 can also be found at: <http://www.gnu.org/licenses/>.

from Bio import SeqIO
from BCBio import GFF
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.SeqFeature import SeqFeature, FeatureLocation
import os, subprocess
import collections, re
import logging
import shutil, pickle, json
from pathlib import Path

def runantismash(working_dir, reference_genome, cpu, multiple):
    asdir = os.path.join(working_dir, "antismash_output")
    if multiple == True:
        parent_dir = Path(working_dir).parent.absolute()
        parent_as_path = os.path.join(parent_dir, "antismash_output")
        if os.path.exists(parent_as_path) and os.path.exists(asdir) != True:
            shutil.move(parent_as_path, working_dir)
    as_gbk_result_file = os.path.join(asdir, Path(reference_genome).stem + ".gbk")
    if os.path.exists(as_gbk_result_file):
        logging.info("Antismash result exist")
        return as_gbk_result_file
    if  os.path.exists(reference_genome):
        if os.path.exists(asdir):
            shutil.rmtree(asdir)
        try:
            logging.info("antismash run started")
            cmd =  "antismash" + " " +  reference_genome + " --cb-knownclusters " + "--output-dir " + asdir + " -v --cpus " + str(cpu) + " --genefinding-tool prodigal"
            logging.info(cmd)
            aslog_path = os.path.join(working_dir, "aslog.txt")
            with open(aslog_path,'w') as aslog:
                subprocess.run([cmd], stderr=aslog, stdout=aslog, shell=True)
            return as_gbk_result_file
        except:
            logging.error("Antismash output not found")
    else:
        logging.error("Could not find Antismash executable and/or input file")
        return





def parse_gbk(gbk_result_file):
    protoclusters = {}
    whole_sequences = {}
    cds_dict = {}
    rec_num = 0
    for seq_record in SeqIO.parse(gbk_result_file, "genbank"):
        rec_num += 1
        id =seq_record.id
        whole_sequences[seq_record.id + "|" + seq_record.description] = seq_record.seq
        reg_num = 0
        for feature in seq_record.features:
            if feature.type == "CDS":
                if "locus_tag" in feature.qualifiers:
                    locus_tag = feature.qualifiers["locus_tag"][0]
                else:
                    locus_tag = "N/A"
                    logging.debug("No locus_tag for %s"%(seq_record.description))
                if "gene_functions" in feature.qualifiers:
                    # if len(feature.qualifiers["gene_functions"]) > 1:
                    #     print("Asd")
                    gene_function = feature.qualifiers["gene_functions"][0]
                else:
                    gene_function = "N/A"
                if "protein_id" in feature.qualifiers:
                    protein_id = feature.qualifiers["protein_id"][0]
                else:
                    protein_id = "N/A"
                    logging.debug("No protein_id for %s"%(seq_record.description))
                if "product" in feature.qualifiers:
                    product = feature.qualifiers["product"][0]
                else:
                    product = "N/A"
                    logging.debug("No product for %s"%(seq_record.description))
                if seq_record.id + "|" + seq_record.description in cds_dict:
                    cds_dict[seq_record.id + "|" + seq_record.description].append([locus_tag, str(feature.location.start).strip(">").strip("<"),
                    str(feature.location.end).strip(">").strip("<"), protein_id, product, gene_function, str(feature.location.strand), id])
                else:
                    cds_dict[seq_record.id + "|" + seq_record.description] = [[locus_tag, str(feature.location.start).strip(">").strip("<"),
                    str(feature.location.end).strip(">").strip("<"), protein_id, product, gene_function, str(feature.location.strand), id]]
            if feature.type == "region":
                reg_num += 1
                reg_id = str(rec_num) + "_" + str(reg_num)
                if "product" in feature.qualifiers:
                    product = "|".join(feature.qualifiers["product"])
                else:
                    product = "N/A"
                    logging.debug("No product for region %s"%(str(feature.location.start).strip(">").strip("<")))
                if seq_record.id + "|" + seq_record.description in protoclusters:
                    protoclusters[seq_record.id + "|" + seq_record.description].append(
                        [reg_id, [str(feature.location.start).strip(">").strip("<"),str(feature.location.end).strip(">").strip("<"),str(feature.location.strand)],
                         product])
                else:
                    protoclusters[seq_record.id + "|" + seq_record.description] = \
                        [[reg_id, [str(feature.location.start).strip(">").strip("<"),str(feature.location.end).strip(">").strip("<"),str(feature.location.strand)],
                         product]]
    return protoclusters,cds_dict,whole_sequences


def convert_gbk(gbk_result_file, working_dir):
    gff_files = os.path.join(working_dir, "gff_gtf")
    if os.path.exists(gff_files) != True:
        os.mkdir(gff_files)
    output_handle_gff3_path = os.path.join(gff_files, "annotations.gff")
    gff_out_handle = open(output_handle_gff3_path, "w")
    gff_out_handle.write("#generated_gff")
    reference_genomes = os.path.join(working_dir, "reference_genome")
    if os.path.exists(reference_genomes) != True:
        os.mkdir(reference_genomes)
    rec_num = 0
    fasta_record_list = []
    fasta_record_list_prot = []
    new_seq_records = []
    for seq_record in SeqIO.parse(gbk_result_file, "genbank"):
        nuc_desc = seq_record.name
        nuc_seq = seq_record.seq
        nuc_rec = SeqRecord(nuc_seq, nuc_desc, "", "")
        fasta_record_list.append(nuc_rec)
        for feature in seq_record.features:
            if feature.type == "CDS":
                if "locus_tag" in feature.qualifiers:
                    prot_desc = feature.qualifiers["locus_tag"][0]
                    gene_start = str(int(feature.location.start) + 1)
                    gene_end = str(int(feature.location.end))
                    cds_start = str(int(feature.location.start) + 1)
                    cds_end = str(int(feature.location.end) -3)
                    strand = str(feature.location.strand).replace("-1", "-").replace("1", "+")
                else:
                    continue
                if "translation" in feature.qualifiers:
                    prot_seq = Seq(feature.qualifiers["translation"][0])
                else:
                    continue
                if "protein_id" not in feature.qualifiers:
                    feature.qualifiers["protein_id"] = [prot_desc]
                if "product" not in feature.qualifiers:
                    feature.qualifiers["product"] = ["unknown_product_" + prot_desc]
                if "gene" not in feature.qualifiers:
                    feature.qualifiers["gene"] = [prot_desc]
                if "GENEID" not in feature.qualifiers:
                    feature.qualifiers["GENEID"] = [prot_desc]
                gff_line_gene = "\n%s\tUnknown_source\tgene\t%s\t%s\t.\t%s\t0\tgene_id \"%s\"; "%(nuc_desc, gene_start, gene_end, strand, prot_desc)
                gff_line_cds  =  "\n%s\tUnknown_source\tCDS\t%s\t%s\t.\t%s\t0\tgene_id \"%s\";"%(nuc_desc, cds_start, cds_end, strand, prot_desc)
                gff_out_handle.write(gff_line_gene)
                gff_out_handle.write(gff_line_cds)
                prot_rec = SeqRecord(prot_seq, prot_desc, "", "")
                fasta_record_list_prot.append(prot_rec)
        new_seq_records.append(seq_record)
    ## write genbank##
    output_handle_new_gbk_path = os.path.join(reference_genomes, "new_genbank_genome.gbk")
    SeqIO.write(new_seq_records, output_handle_new_gbk_path, "genbank")
    ## write genome##
    output_handle_genome_fna_path = os.path.join(reference_genomes, "genome.fna")
    SeqIO.write(fasta_record_list, output_handle_genome_fna_path, "fasta")
    ## write protein##
    output_handle_prot_faa_path = os.path.join(reference_genomes, "proteins.faa")
    SeqIO.write(fasta_record_list_prot, output_handle_prot_faa_path, "fasta")
    ## write gff##
    gff_out_handle.close()
    return output_handle_gff3_path, output_handle_genome_fna_path, output_handle_new_gbk_path, output_handle_prot_faa_path, output_handle_new_gbk_path




def check_pos(gbk_file, working_dir, regulator_list, regulator_dict,
              resistance_list, resistance_dict,  master_dict, user_cluster, housekeeping_list, housekeeping_dict):
    parsed_json = parse_json(working_dir)
    final_resul_file_all = open(os.path.join(working_dir, "all_antismash_positions"),"w")
    final_resul_file_reg = open(os.path.join(working_dir, "regulator_antismash_positions"), "w")
    protoclusters,cds_dict,whole_sequences = parse_gbk(gbk_file)
    intersection_proteins = []
    if user_cluster != None:
        users_gene_dict = {}
        defined_clusters = open(user_cluster, "r")
        try:
            for line in defined_clusters:
                if line.startswith("#"):
                    if "||" in line:
                        users_cluster_name = line.split("||")[1].strip()
                    else:
                        users_cluster_name = "Users_cluster_unknown"
                    users_cluster_name = users_cluster_name.replace(" ", "_")
                    users_cluster_name = (re.sub(r"[^a-zA-Z0-9]+", '_', users_cluster_name))
                else:
                    gene_id = line.strip()
                    users_gene_dict[gene_id] = users_cluster_name
        except:
            logging.ERROR("no user cluster or faulty file")
            print("no user cluster or faulty file")
    for sequence in cds_dict:
        if sequence not in protoclusters.keys():
            for gene in cds_dict[sequence]:
                gene_name = gene[0]
                gene_start = gene[1]
                gene_end = gene[2]
                protein_id = gene[3]
                product = gene[4]
                gene_function = gene[5]
                strand = gene[6]
                id = gene[7]
                if gene_name not in master_dict:
                    logging.error("gene name %s not in master dict, probably generated by antismash"%gene_name)
                    master_dict[gene_name] = {}
                    # continue
                master_dict[gene_name]['id'] = id
                if housekeeping_list == []:
                    master_dict[gene_name]["housekeeping"] = "N/A"
                else:
                    if protein_id in housekeeping_list:
                        master_dict[gene_name]["housekeeping"] = housekeeping_dict[protein_id]
                    elif gene_name in housekeeping_list:
                        master_dict[gene_name]["housekeeping"] = housekeeping_dict[gene_name]
                    else:
                        master_dict[gene_name]["housekeeping"] = "false"
                if protein_id in regulator_list:
                    master_dict[gene_name]["regulator"] = regulator_dict[protein_id]
                else:
                    master_dict[gene_name]["regulator"] = "N/A"
                if protein_id in resistance_list:
                    master_dict[gene_name]["resistance"] = resistance_dict[protein_id]
                else:
                    master_dict[gene_name]["resistance"] = "N/A"
                master_dict[gene_name]["protein_name"] = protein_id
                master_dict[gene_name]["product"] = product
                master_dict[gene_name]["gene_function"] = gene_function
                master_dict[gene_name]["gene_start"] = gene_start
                master_dict[gene_name]["gene_end"] = gene_end
                master_dict[gene_name]["strand"] = strand
            continue
        else:
            for gene in cds_dict[sequence]:
                gene_name = gene[0]
                gene_start = gene[1]
                gene_end = gene[2]
                protein_id = gene[3]
                product = gene[4]
                gene_function = gene[5]
                strand = gene[6]
                id = gene[7]
                if gene_name not in master_dict:
                    logging.error("gene name %s not in master dict, probably generated by antismash"%gene_name)
                    master_dict[gene_name] = {}
                    # continue
                master_dict[gene_name]['id'] = id
                if housekeeping_list == []:
                    master_dict[gene_name]["housekeeping"] = "N/A"
                else:
                    if protein_id in housekeeping_list:
                        master_dict[gene_name]["housekeeping"] = housekeeping_dict[protein_id]
                    elif gene_name in housekeeping_list:
                        master_dict[gene_name]["housekeeping"] = housekeeping_dict[gene_name]
                    else:
                        master_dict[gene_name]["housekeeping"] = "false"
                if protein_id in regulator_list:
                    master_dict[gene_name]["regulator"] = regulator_dict[protein_id]
                else:
                    master_dict[gene_name]["regulator"] = "N/A"
                if protein_id in resistance_list:
                    master_dict[gene_name]["resistance"] = resistance_dict[protein_id]
                else:
                    master_dict[gene_name]["resistance"] = "N/A"
                master_dict[gene_name]["protein_name"] = protein_id
                master_dict[gene_name]["product"] = product
                master_dict[gene_name]["gene_function"] = gene_function
                master_dict[gene_name]["gene_start"] = gene_start
                master_dict[gene_name]["gene_end"] = gene_end
                master_dict[gene_name]["strand"] = strand
                for cluster in protoclusters[sequence]:
                    cluster_start = cluster[1][0]
                    cluster_end = cluster[1][1]
                    if int(gene_start) >= int(cluster_start) and int(gene_end) <= int(cluster_end):
                        if protein_id in regulator_list:
                            intersection_proteins.append(protein_id)
                            protocluster_number = cluster[0]
                            products = cluster[-1]
                            dna_sequence = whole_sequences[sequence][int(gene_start):int(gene_end)]
                            final_resul_file_reg.write(">" + sequence + "|" + gene_name + "|" + protein_id + "|" + protocluster_number + "|" \
                                                   + "_".join(products) + "|" + str(cluster_start) + "|" + str(cluster_end) + "|" + \
                                                   str(gene_start) + "|" + str(gene_end) + \
                                                   "\n" + str(dna_sequence) + "\n")

                        protocluster_number = cluster[0]
                        products = cluster[-1]
                        dna_sequence = whole_sequences[sequence][int(gene_start):int(gene_end)]
                        final_resul_file_all.write(">" + sequence + "|" + gene_name + "|" + protein_id + "|" + protocluster_number + "|" \
                                               + "_".join(products) + "|" + str(cluster_start) + "|" + str(cluster_end) + "|" + \
                                               str(gene_start) + "|" + str(gene_end) + \
                                               "\n" + str(dna_sequence) + "\n")
                        master_dict[gene_name]["bgc_prox"] = "true"
                        master_dict[gene_name]["region_number"] = protocluster_number
                        master_dict[gene_name]["known_cluster"] = parsed_json[protocluster_number]
                        break
                    else:
                        master_dict[gene_name]["bgc_prox"] = "false"
                        master_dict[gene_name]["region_number"] = "N/A"
                        master_dict[gene_name]["known_cluster"] = "N/A"
                if user_cluster != None and gene_name in users_gene_dict:
                    master_dict[gene_name]["gene_function"] = "biosynthetic"
                    master_dict[gene_name]["bgc_prox"] = "true"
                    master_dict[gene_name]["region_number"] = users_gene_dict[gene_name]
                    master_dict[gene_name]["known_cluster"] = users_gene_dict[gene_name]
    final_resul_file_all.close()
    final_resul_file_reg.close()
    return master_dict

def master_gene_dict(gtf_file):
   gtf = open(gtf_file, "r")
   master_dict = {}
   xc = 0
   for line in gtf:
       if line.startswith("#"):
           continue
       if line.split("\t")[2] == "gene":
          if "GeneID" in line:
              GeneID = line.split("GeneID:")[1].split("\"")[0]
          else:
              GeneID = "N/A"
              xc += 1
              if xc < 2:
                  print("WARNING NO GENEID")
                  logging.error("NO GENEID")
          gene_id = line.split("gene_id")[1].split("\"")[1]
          master_dict[gene_id] = {}
          master_dict[gene_id]["GeneID"] = GeneID
          master_dict[gene_id]["bgc_prox"] = "N/A"
   return master_dict

def gene_ids(gtf_file):
   gtf = open(gtf_file, "r")
   gene_id_dict = {}
   for line in gtf:
      if line.startswith("#"):
        continue
      if line.split("\t")[2] == "gene":
          if "GeneID" in line:
             GeneID = line.split("GeneID:")[1].split("\"")[0]
          else:
              GeneID = "N/A"
          gene_id = line.split("gene_id")[1].split("\"")[1]
          gene_id_dict [gene_id] = GeneID
   # print(gene_id_dict)
   return gene_id_dict

def search_annotation(working_dir, gtf_file, master_dict):
    go_terms_dict = {}
    gene_id_dict = gene_ids(gtf_file)
    deseq_out = open(os.path.join(working_dir, "deseq_out.tsv"), "r")
    for line in deseq_out:
        gene_id = "N/A"
        if line.startswith("\"baseMean"):
            continue
        gene = line.split()[0].strip("\"")
        fold_change = line.split()[2].strip()
        padj = line.split()[-1].strip()
        master_dict[gene]["fold_change"] = fold_change
        master_dict[gene]["padj"] = padj
    return master_dict


def parse_json(working_dir):
    import glob
    print(os.path.join(working_dir,"antismash_output"))
    json_path = glob.glob(os.path.join(working_dir,"antismash_output")+'/*.json')[0]
    print(json_path)
    known_cluster_count_dict = {}
    with open(json_path) as f:
        loaded_json = json.load(f)
        # print(loaded_json)
        rec_num = 0
        record_to_known = {}
        for record in loaded_json["records"]:
            rec_num += 1
            try:
                for known_cluster_result in record["modules"]['antismash.modules.clusterblast']["knowncluster"]["results"]:
                    #print(known_cluster_result)
                    if known_cluster_result["total_hits"] == 0:
                        record_to_known[str(rec_num) + "_" + str(known_cluster_result["region_number"])] = "N/A"
                    else:
                        if known_cluster_result["ranking"][0][0]["description"] not in known_cluster_count_dict:
                            known_cluster_count_dict[known_cluster_result["ranking"][0][0]["description"]] = 1
                            record_to_known[str(rec_num) + "_" + str(known_cluster_result["region_number"])] = known_cluster_result["ranking"][0][0]["description"]
                        else:
                            known_cluster_count_dict[known_cluster_result["ranking"][0][0]["description"]] += 1
                            record_to_known[str(rec_num) + "_" + str(known_cluster_result["region_number"])] = known_cluster_result["ranking"][0][0]["description"] + "_#" + str(known_cluster_count_dict[known_cluster_result["ranking"][0][0]["description"]])
            except Exception as e:
                print(e)
    return record_to_known


if __name__ == '__main__':
    print("main")