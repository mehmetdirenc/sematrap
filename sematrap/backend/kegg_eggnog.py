#  Copyright (C) 2021,2022 Mehmet Direnc Mungan
#  University of Tuebingen
#  Interfaculty Institute of Microbiology and Infection Medicine
#  Lab of Nadine Ziemert, Div. of Microbiology/Biotechnology
#  Funding by the German Centre for Infection Research (DZIF)
#  #
#  This file is part of SeMa-Trap
#  SeMa-Trap is free software. you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  #
#  #This program is distributed in the hope that it will be useful,
#   #but WITHOUT ANY WARRANTY; without even the implied warranty of
#   #MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   #GNU General Public License for more details.
#
#  License: You should have received a copy of the GNU General Public License v3 with SeMa-Trap
#  A copy of the GPLv3 can also be found at: <http://www.gnu.org/licenses/>.

import os, subprocess
import json, logging
import pickle



def run_emapper(working_dir, genome_name, protein_file, multiple, cores, egg_data_dir):
    from pathlib import Path
    import shutil
    eggnog_dir = os.path.join(working_dir, "eggnog_results")
    if multiple == True:
        parent_dir = Path(working_dir).parent.absolute()
        parent_eg_path = os.path.join(parent_dir, "eggnog_results")
        if os.path.exists(parent_eg_path):
            shutil.move(parent_eg_path, working_dir)
    if os.path.exists(eggnog_dir) != True:
        os.mkdir(eggnog_dir)
    else:
        return
    logging.info("emapper.py --cpu %s --itype proteins -i %s "
                    "-o %s --output_dir %s --data_dir \"%s\""%(str(cores), protein_file, genome_name, eggnog_dir, egg_data_dir))
    subprocess.call(["emapper.py --cpu %s --itype proteins -i %s "
                    "-o %s --output_dir %s --data_dir \"%s\""%(str(cores), protein_file, genome_name, eggnog_dir, egg_data_dir)], shell=True)

def parse_eggnog(working_dir):
    eggnog_results = os.path.join(working_dir, "eggnog_results")
    annotation_file = open(os.path.join(eggnog_results, [f for f in os.listdir(eggnog_results) if f.endswith(".annotations")][0]), "r")
    prot_annotation_dict = {}
    for line in annotation_file:
        if line.startswith("#"):
            continue
        split_line = line.split("\t")
        prot_name = split_line[0]
        prot_annotation_dict[prot_name]= {}
        ko_nums =[k.replace("ko:", "") for k in split_line[11].split(",")]
        pathways = [p for p in split_line[12].split(",") if p.startswith("map") != True]
        reactions = split_line[14].split(",")
        brites = [b for b in split_line[16].split(",") if b.startswith("ko")]
        prot_annotation_dict[prot_name]["ko_nums"] = ko_nums
        prot_annotation_dict[prot_name]["pathways"] = pathways
        prot_annotation_dict[prot_name]["reactions"] = reactions
        prot_annotation_dict[prot_name]["brites"] = brites
        if "ko01130" in pathways or "ko01110" in pathways:
            prot_annotation_dict[prot_name]["secondary"] = "true"
        else:
            prot_annotation_dict[prot_name]["secondary"] = "false"
    return prot_annotation_dict



if __name__ == '__main__':
    print("main")
