#  Copyright (C) 2021,2022 Mehmet Direnc Mungan
#  University of Tuebingen
#  Interfaculty Institute of Microbiology and Infection Medicine
#  Lab of Nadine Ziemert, Div. of Microbiology/Biotechnology
#  Funding by the German Centre for Infection Research (DZIF)
#  #
#  This file is part of SeMa-Trap
#  SeMa-Trap is free software. you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  #
#  #This program is distributed in the hope that it will be useful,
#   #but WITHOUT ANY WARRANTY; without even the implied warranty of
#   #MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   #GNU General Public License for more details.
#
#  License: You should have received a copy of the GNU General Public License v3 with SeMa-Trap
#  A copy of the GPLv3 can also be found at: <http://www.gnu.org/licenses/>.

import os, subprocess
import logging

def run_regulators_hmm(working_dir, cpu, hmm_file, cds_file):
    out_file = os.path.join(working_dir, "regulators.tblout")
    hmm_file = os.path.abspath(hmm_file)
    if os.path.exists(out_file):
        return out_file
    subprocess.call(["hmmsearch", "-o", out_file + "temp", "--noali", "--cpu", str(cpu), "--cut_tc", "--notextw", "--tblout", out_file, hmm_file, cds_file])
    os.remove(out_file + "temp")
    return out_file


def parse_regulators_hmm_result(hmm_result):
    res = open(hmm_result, "r")
    regulators_dict = {}
    for line in res:
        if line.startswith("# Domain scores"):
            break
        elif line.startswith("#"):
            continue
        prot = line[0:19].strip()
        regulator = line[32:52].strip()
        regulators_dict[prot] = regulator
    regulators_list = list(regulators_dict.keys())
    return regulators_list, regulators_dict

def run_resistance_hmm(working_dir, cpu, hmm_file, cds_file):
    out_file = os.path.join(working_dir, "resistance.tblout")
    if os.path.exists(out_file):
        return out_file
    subprocess.call(["hmmsearch", "-o", out_file + "temp", "--noali", "--cpu", str(cpu), "--cut_tc", "--notextw", "--tblout", out_file, hmm_file, cds_file])
    os.remove(out_file + "temp")
    return out_file


def parse_resistance_hmm_result(hmm_result):
    res = open(hmm_result, "r")
    resistance_dict = {}
    for line in res:
        if line.startswith("# Domain scores"):
            break
        elif line.startswith("#"):
            continue
        prot = line[0:19].strip()
        resistance = line[32:52].strip()
        resistance_dict[prot] = resistance
    resistance_list = list(resistance_dict.keys())
    return resistance_list, resistance_dict


def parse_pfams(pfam_file, new_file, keywords):
    new_fams = {}
    with open(pfam_file, "r") as pfams:
        x = ""
        write = True
        for line in pfams:
            if "HMMER" in line:
                write = True
            if "ACC  " in line:
                for keyword in keywords:
                    if keyword in line:
                        desc = line.strip("\n")
                        write = True
                        break
                else:
                    write = False
                    x = ""
            if write:
                x += line
            else:
                continue
            if line == "//\n":
                new_fams[desc] = x
                x = ""
    with open(new_file, "w") as new_pfams:
        for i in new_fams:
            new_pfams.write(new_fams[i])


def housekeeping_gene_search(working_dir, refset, refset_path, protein_filename, cores):
    out_file = os.path.join(working_dir, "housekeeping.tblout")
    hmm_file = os.path.join(refset_path,refset, "coremodels_exp.hmm")
    if os.path.exists(out_file):
        return out_file
    logging.info("hmmsearch"+ " -o "+ out_file + "temp "+ " --noali "+ " --cpu " + str(cores)+ " --cut_tc "+ " --notextw "+ " --tblout "+ out_file +" "+ hmm_file+" "+ protein_filename)
    subprocess.call(["hmmsearch", "-o", out_file + "temp", "--noali", "--cpu", str(cores), "--cut_tc", "--notextw", "--tblout", out_file, hmm_file, protein_filename])
    os.remove(out_file + "temp")
    return out_file

def parse_housekeeping_hmm_result(hmm_result):
    res = open(hmm_result, "r")
    housekeeping_dict = {}
    for line in res:
        if line.startswith("#"):
            continue
        prot = line.split("-")[0].strip()
        name = line.split("-")[1].split()[0].strip()
        housekeeping_dict[prot] = name
    housekeeping_list = list(housekeeping_dict.keys())
    return housekeeping_list, housekeeping_dict


if __name__ == '__main__':
    print("main")