#  Copyright (C) 2021,2022 Mehmet Direnc Mungan
#  University of Tuebingen
#  Interfaculty Institute of Microbiology and Infection Medicine
#  Lab of Nadine Ziemert, Div. of Microbiology/Biotechnology
#  Funding by the German Centre for Infection Research (DZIF)
#  #
#  This file is part of SeMa-Trap
#  SeMa-Trap is free software. you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  #
#  #This program is distributed in the hope that it will be useful,
#   #but WITHOUT ANY WARRANTY; without even the implied warranty of
#   #MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   #GNU General Public License for more details.
#
#  License: You should have received a copy of the GNU General Public License v3 with SeMa-Trap
#  A copy of the GPLv3 can also be found at: <http://www.gnu.org/licenses/>.
#
#  License: You should have received a copy of the GNU General Public License v3 with SeMa-Trap
#  A copy of the GPLv3 can also be found at: <http://www.gnu.org/licenses/>.

import os, subprocess, json, threading
import logging, shutil

def download_fastq(input_file, working_dir, paired_or_single):
    fastq_dir = os.path.join(working_dir, "fastq_files")
    if os.path.exists(fastq_dir) != True:
        os.mkdir(fastq_dir)
    with open(input_file, "r") as inp:
        all_lines = inp.readlines()
        for line in all_lines:
            if line == "\n":
                continue
            acc = line.split("\t")[0].strip()
            condition = line.split("\t")[2].strip()
            final_dir = os.path.join(fastq_dir, condition)
            # fastq_ftp_path = check_read_ftp(working_dir, acc)
            if os.path.exists(final_dir) != True:
                os.mkdir(final_dir)
            # if acc.startswith("SRP"):
            #     srr = subprocess.check_output(["curl \'https://trace.ncbi.nlm.nih.gov/Traces/sra/sra."
            #                                    "cgi?save=efetch&db=sra&rettype=runinfo&term=%s\' |"
            #                                    " grep %s | cut -f1 -d\",\""
            #                                    % (acc, acc)], shell=True)
            #     srr = srr.decode("utf-8").strip()
            #     logging.info(srr)
            #     download_from_sra_acc(srr,final_dir)
            # else:
            # out_path = os.path.join(final_dir, os.path.basename(acc))
            # cmd = "wget -O \"%s\" \"%s\"" %(out_path, fastq_ftp_path)
            # command = check_sra(cmd)
            # sra_bool = command.run2(timeout=5, fpath=out_path)
            # if os.path.exists(out_path):
            #     os.remove(out_path)
            # if fastq_ftp_path == False or sra_bool == False:
            logging.info(acc)
            download_from_sra_acc(acc, final_dir)
            # else:
            #     print("wgetteyiz")
            #     download_from_wget(fastq_ftp_path, final_dir, paired_or_single)
            for folder in os.listdir(final_dir):
                if os.path.isdir(os.path.join(final_dir, folder)):
                    for file in os.listdir(os.path.join(final_dir, folder)):
                        file_path = os.path.join(final_dir,folder, file)
                        subprocess.call("gunzip " + file_path, shell=True)
                    for file in os.listdir(os.path.join(final_dir, folder)):
                        file_path = os.path.join(final_dir, folder, file)
                        shutil.move(file_path, os.path.join(final_dir))
                        os.removedirs(os.path.join(final_dir, folder))

    return final_dir

def check_read_ftp(working_dir, accession):

    template = "https://www.ebi.ac.uk/ena/portal/api/filereport?accession=%s&result=read_run&" \
               "fields=fastq_ftp,submitted_ftp,sra_ftp&format=json&download=true&limit=0"%accession
    try:
        path = os.path.join(working_dir, accession +".json")
        subprocess.call("wget -O \"%s\" \"%s\""%(path,template), shell=True)
        with open(path) as reads:
            config_dict = json.load(reads)
        ftp_path = config_dict[0]["fastq_ftp"]
        if ftp_path == "":
            return False
        else:
            return ftp_path
    except:
        return False

def download_from_wget(acc, output_dir, paired_or_single):
    logging.info("Downloading from wget")
    if paired_or_single == "s":
        logging.info("wget -O \"%s\" \"%s\"" %(output_dir, acc))
        out_path = os.path.join(output_dir, os.path.basename(acc))
        cmd = "wget -O \"%s\" \"%s\"" %(out_path, acc)
        print("download_wget",cmd)
        subprocess.call(["wget -O \"%s\" \"%s\"" %(out_path, acc)], shell=True)
    if paired_or_single == "pe":
        ftps = acc.split(";")
        for ftp in ftps:
            logging.info("wget -O \"%s\" \"%s\"" % (output_dir, ftp))
            subprocess.call(["wget -O \"%s\" \"%s\"" % (output_dir, ftp)], shell=True)



class check_sra(object):
    def __init__(self, cmd):
        self.cmd = cmd
        self.process = None

    def run2(self, timeout, fpath):
        def target():
            # print("started", self.cmd)
            self.process = subprocess.Popen(self.cmd, shell=True)
            self.process.communicate()
            # return False
        thread = threading.Thread(target=target)
        thread.start()
        thread.join(timeout)
        if thread.is_alive():
            self.process.terminate()
            print(self.cmd)
            cmd_kill = "pkill -9 -f \"%s\""%fpath[1:-1]
            subprocess.call(cmd_kill, shell=True)
            b = os.path.getsize(fpath)
            if int(b) > 1:
                return True
            else:
                return False
        else:
            return False


def check_filesize(cmd, fpath):
    # cmd = "wget -O \"%s\" \"%s\"" %(out_path, acc)
    command = check_sra(cmd)
    wget_bool = command.run2(timeout=5, fpath=fpath)
    # sra_bool = command.run2(timeout=3)

def download_from_sra_acc(acc, output_dir):
    ##TODO##
    ##change this path##
    logging.info("fastq-dump --split-3 %s -O \"%s\"" % (acc, output_dir))
    # subprocess.call(["/media/ecoli/2gb/transcriptomics/sratoolkit.2.11.1-ubuntu64/bin/fastq-dump --split-3 %s -O \"%s\"" % (acc, output_dir)], shell=True)
    subprocess.call(["fastq-dump-orig.2.11.0 --split-3 %s -O \"%s\"" % (acc, output_dir)], shell=True)

def check_read_availability(working_dir, accession):

    template = "https://www.ebi.ac.uk/ena/portal/api/filereport?accession=%s&result=read_run&" \
               "fields=fastq_ftp,submitted_ftp,sra_ftp&format=json&download=true&limit=0"%accession
    path = os.path.join(working_dir, accession +".json")
    try:
        subprocess.call("wget -O \"%s\" \"%s\""%(path,template), shell=True)
        with open(path) as reads:
            config_dict = json.load(reads)
        if len(config_dict) < 1:
            os.remove(path)
            return False
        else:
            os.remove(path)
            return True
    except:
        if os.path.exists(path):
            os.remove(path)
        return False

if __name__ == '__main__':
    print("main")