#  Copyright (C) 2021,2022 Mehmet Direnc Mungan
#  University of Tuebingen
#  Interfaculty Institute of Microbiology and Infection Medicine
#  Lab of Nadine Ziemert, Div. of Microbiology/Biotechnology
#  Funding by the German Centre for Infection Research (DZIF)
#  #
#  This file is part of SeMa-Trap
#  SeMa-Trap is free software. you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  #
#  #This program is distributed in the hope that it will be useful,
#   #but WITHOUT ANY WARRANTY; without even the implied warranty of
#   #MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   #GNU General Public License for more details.
#
#  License: You should have received a copy of the GNU General Public License v3 with SeMa-Trap
#  A copy of the GPLv3 can also be found at: <http://www.gnu.org/licenses/>.

import os,json, subprocess, pickle, statistics, logging, pathlib
from pathlib import Path
from multiprocessing import Pool as ThreadPool
from sematrap.backend.kegg_eggnog import parse_eggnog
from shutil import copy2

def final_results(working_dir):
    with open(os.path.join(working_dir, "master_dict.json")) as f:
        master_dict = json.load(f)
    final_parsed_dir = os.path.join(working_dir, "final_results")
    if os.path.exists(final_parsed_dir) != True:
        os.mkdir(final_parsed_dir)
    regs_file = open(os.path.join(final_parsed_dir, "regulators.json"), "w")
    res_file = open(os.path.join(final_parsed_dir, "resistance.json"), "w")
    regulators = {}
    resistance_genes = {}
    bgcs = {}

    for i in master_dict:
        try:
            if master_dict[i]["regulator"] != "N/A":
                regulators[i] = master_dict[i]
            if master_dict[i]["resistance"] != "N/A":
                resistance_genes[i] = master_dict[i]
        except:
            continue
        if master_dict[i]["bgc_prox"] != "false":
            if master_dict[i]["known_cluster"] == "N/A":
                if master_dict[i]["region_number"] in bgcs:
                    bgcs[master_dict[i]["region_number"]]["genes"].append(master_dict[i])
                else:
                    bgcs[master_dict[i]["region_number"]] = {"genes" : [master_dict[i]]}
            else:
                if master_dict[i]["known_cluster"] in bgcs:
                    bgcs[master_dict[i]["known_cluster"]]["genes"].append(master_dict[i])
                else:
                    bgcs[master_dict[i]["known_cluster"]] = {"genes" : [master_dict[i]]}
    for i in bgcs:
        json.dump(bgcs[i], open(os.path.join(final_parsed_dir, i.replace(" ", "_").replace("/","-")), "w"), indent=4)
    json.dump(regulators, regs_file, indent=4)
    json.dump(resistance_genes, res_file, indent=4)


def generate_regulators(working_dir):
    with open(os.path.join(working_dir, "master_dict.json")) as f:
        master_dict = json.load(f)
    final_parsed_dir = os.path.join(working_dir, "final_results")
    if os.path.exists(final_parsed_dir) != True:
        os.mkdir(final_parsed_dir)
    final_reg_dir = os.path.join(final_parsed_dir, "regulators")
    if os.path.exists(final_reg_dir) != True:
        os.mkdir(final_reg_dir)
    regs_file = open(os.path.join(final_reg_dir, "regulators.json"), "w")
    regulators = {}
    for i in master_dict:
        try:
            if master_dict[i]["regulator"] != "N/A":
                regulators[i] = master_dict[i]
        except:
            continue
    json.dump(regulators, regs_file, indent=4)

def generate_resistance_genes(working_dir):
    with open(os.path.join(working_dir, "master_dict.json")) as f:
        master_dict = json.load(f)
    final_parsed_dir = os.path.join(working_dir, "final_results")
    if os.path.exists(final_parsed_dir) != True:
        os.mkdir(final_parsed_dir)
    final_res_dir = os.path.join(final_parsed_dir, "resistance")
    if os.path.exists(final_res_dir) != True:
        os.mkdir(final_res_dir)
    res_file = open(os.path.join(final_res_dir, "resistance.json"), "w")
    resistance_genes = {}
    for i in master_dict:
        try:
            if master_dict[i]["resistance"] != "N/A":
                resistance_genes[i] = master_dict[i]
        except:
            continue
    json.dump(resistance_genes, res_file, indent=4)

def generate_bed_files(working_dir):
    with open(os.path.join(working_dir, "master_dict.json")) as f:
        master_dict = json.load(f)
    final_parsed_dir = os.path.join(working_dir, "bed_files")
    if os.path.exists(final_parsed_dir) != True:
        os.mkdir(final_parsed_dir)
    bgcs = {}
    for i in master_dict:
        try:
            if master_dict[i]["bgc_prox"] != "false":
                if master_dict[i]["known_cluster"] == "N/A":
                    if master_dict[i]["region_number"] in bgcs:
                        bgcs[master_dict[i]["region_number"]]["genes"].append([master_dict[i]["id"], master_dict[i]["gene_start"], master_dict[i]["gene_end"]])
                    else:
                        bgcs[master_dict[i]["region_number"]] = {"genes": [[master_dict[i]["id"], master_dict[i]["gene_start"], master_dict[i]["gene_end"]]]}
                else:
                    if master_dict[i]["known_cluster"] in bgcs:
                        bgcs[master_dict[i]["known_cluster"]]["genes"].append([master_dict[i]["id"], master_dict[i]["gene_start"], master_dict[i]["gene_end"]])
                    else:
                        bgcs[master_dict[i]["known_cluster"]] = {"genes": [[master_dict[i]["id"], master_dict[i]["gene_start"], master_dict[i]["gene_end"]]]}
        except Exception as e:
            continue
    for i in bgcs:
        with open(os.path.join(final_parsed_dir, i.replace(" ", "_").replace("/", "-") + ".bed"), "w") as bed:
            for pos in bgcs[i]["genes"]:
                bed.write("\t".join(pos) + " \n")

def run_mpileup(bed_bam_files):
    bed = bed_bam_files.split("|")[0]
    bam = bed_bam_files.split("|")[2]
    working_dir = bed_bam_files.split("|")[1]
    if os.path.exists(os.path.join(working_dir, "mpileup_results")) != True:
        os.mkdir(os.path.join(working_dir, "mpileup_results"))
    out = os.path.join(working_dir, "mpileup_results", os.path.basename(bed) + "--" + os.path.basename(bam) + "_mpileup")
    if os.path.exists(out) != True:
        subprocess.call("samtools mpileup -l %s %s > %s"%(bed, bam, out), shell=True)

def generate_mpileup_results(working_dir, cpu):
    full_array = []
    bed_files_path = os.path.join(working_dir, "bed_files")
    bed_files = os.listdir(bed_files_path)
    bam_files_path = os.path.join(working_dir, "hisat2_results")
    bam_files = os.listdir(bam_files_path)
    for i in bed_files:
        bed_path = os.path.join(bed_files_path,i)
        for y in bam_files:
            bam_path = os.path.join(bam_files_path, y)
            full_array.append(bed_path + "|" + working_dir + "|" + bam_path + "|" + str(cpu))
    pool = ThreadPool(cpu)
    pool.map(run_mpileup, full_array)


def annotation_tree(the_path, eggnog_ko_number, gene_name, desc, fc, secondary,  keggs_dict_secondary, keggs_dict_others):
    try:
        met = the_path.split("||")[3].split("|-|")[1]
        met_number = the_path.split("||")[3].split("|-|")[0]
        child_met = the_path.split("||")[2].split("|-|")[1]
        child_met_number = the_path.split("||")[2].split("|-|")[0]
        grand_child_met = the_path.split("||")[1].split("|-|")[1]
        grand_child_met_number = the_path.split("||")[1].split("|-|")[0]
    except:
        print(the_path)
    if secondary == "true":
        if met not in keggs_dict_secondary:
            keggs_dict_secondary[met] = {"id": met_number, "count": 1,
                               child_met:
                                   {"child_id": child_met_number, "count": 1,
                                    grand_child_met:
                                        {
                                            "grand_child_id": grand_child_met_number, "count": 1,
                                            eggnog_ko_number:
                                                {
                                                    "description": desc,
                                                    "count": 1, "genes": [gene_name + "||"+ str(fc)]
                                                }
                                        }
                                    }

                               }
        else:
            keggs_dict_secondary[met]["count"] += 1
            if child_met not in keggs_dict_secondary[met]:
                keggs_dict_secondary[met][child_met] = {"child_id": child_met_number, "count": 1}
            else:
                keggs_dict_secondary[met][child_met]["count"] += 1
                if grand_child_met not in keggs_dict_secondary[met][child_met]:
                    keggs_dict_secondary[met][child_met][grand_child_met] = \
                        {"grand_child_id": grand_child_met_number, "count": 1}
                else:
                    keggs_dict_secondary[met][child_met][grand_child_met]["count"] += 1
                    if eggnog_ko_number not in keggs_dict_secondary[met][child_met][grand_child_met]:
                        keggs_dict_secondary[met][child_met][grand_child_met][eggnog_ko_number] = \
                            {"count": 1, "description": desc, "genes": [gene_name + "||"+ str(fc)]}
                    else:
                        keggs_dict_secondary[met][child_met][grand_child_met][eggnog_ko_number]["count"] += 1
                        keggs_dict_secondary[met][child_met][grand_child_met][eggnog_ko_number]["genes"].append(gene_name + "||"+ str(fc))
    else:
        if met not in keggs_dict_others:
            keggs_dict_others[met] = {"id": met_number, "count": 1,
                                         child_met:
                                             {"child_id": child_met_number, "count": 1,
                                              grand_child_met:
                                                  {
                                                      "grand_child_id": grand_child_met_number, "count": 1,
                                                      eggnog_ko_number:
                                                          {
                                                              "description": desc,
                                                              "count": 1, "genes": [gene_name + "||" + str(fc)]
                                                          }
                                                  }
                                              }

                                         }
        else:
            keggs_dict_others[met]["count"] += 1
            if child_met not in keggs_dict_others[met]:
                keggs_dict_others[met][child_met] = {"child_id": child_met_number, "count": 1}
            else:
                keggs_dict_others[met][child_met]["count"] += 1
                if grand_child_met not in keggs_dict_others[met][child_met]:
                    keggs_dict_others[met][child_met][grand_child_met] = \
                        {"grand_child_id": grand_child_met_number, "count": 1}
                else:
                    keggs_dict_others[met][child_met][grand_child_met]["count"] += 1
                    if eggnog_ko_number not in keggs_dict_others[met][child_met][grand_child_met]:
                        keggs_dict_others[met][child_met][grand_child_met][eggnog_ko_number] = \
                            {"count": 1, "description": desc, "genes": [gene_name + "||" + str(fc)]}
                    else:
                        keggs_dict_others[met][child_met][grand_child_met][eggnog_ko_number]["count"] += 1
                        keggs_dict_others[met][child_met][grand_child_met][eggnog_ko_number]["genes"].append(
                            gene_name + "||" + str(fc))


def generate_kegg_groups(working_dir):
    prot_annotation_dict = parse_eggnog(working_dir)
    with open(os.path.join(working_dir, "master_dict.json")) as f:
        master_dict = json.load(f)
    final_parsed_dir = os.path.join(working_dir, "final_results")
    if os.path.exists(final_parsed_dir) != True:
        os.mkdir(final_parsed_dir)
    keg_pickle_path = os.path.join(pathlib.Path(__file__).parent.parent.resolve(), "static/keg.pickle")
    with open(keg_pickle_path, "rb") as k:
        general_kegg_paths = pickle.load(k)
    kegg_dir = os.path.join(final_parsed_dir,"kegg")
    if os.path.exists(kegg_dir) != True:
        os.mkdir(kegg_dir)
    keggs_dict_secondary = {}
    keggs_dict_others = {}
    for i in master_dict:
        if "padj" in master_dict[i]:
            padj = master_dict[i]["padj"]
        else:
            padj = "N/A"
            fc = "N/A"
        if padj != "NA" and padj != "N/A" :
            if float(padj) > 0.05:
                fc = "not significant"
            else:
                fc = master_dict[i]["fold_change"]
        master_dict[i]["annotations"] = []
        eggnog_brite = False
        eggnog_pathway = False
        neither = True
        if "protein_name" not in master_dict[i]:
            continue
        else:
            protein_name = master_dict[i]["protein_name"]
            if protein_name not in prot_annotation_dict:
                master_dict[i]["annotations"] = "N/A"
                master_dict[i]["reactions"] = "N/A"
                master_dict[i]["secondary"] = "N/A"
                continue
            if prot_annotation_dict[protein_name]["secondary"] == "true":
                master_dict[i]["secondary"] = "true"
                secondary = "true"
            else:
                secondary = "false"
                master_dict[i]["secondary"] = "false"
            if prot_annotation_dict[protein_name]["ko_nums"] == ['-']:
                master_dict[i]["annotations"] = "None"
                if prot_annotation_dict[protein_name]["reactions"] == ['-']:
                    master_dict[i]["reactions"] = "None"
                    continue
                else:
                    master_dict[i]["reactions"] = prot_annotation_dict[protein_name]["reactions"]
                    continue
            else:
                eggnog_ko_nums = prot_annotation_dict[protein_name]["ko_nums"]
            if prot_annotation_dict[protein_name]["pathways"] == ['-']:
                eggnog_brite = True
                eggnog_brite_ids = prot_annotation_dict[protein_name]["brites"]
            else:
                eggnog_pathway = True
                eggnog_pathway_ids = prot_annotation_dict[protein_name]["pathways"]
            for eggnog_ko_number in eggnog_ko_nums:
                if eggnog_ko_number in general_kegg_paths:
                    possible_paths = general_kegg_paths[eggnog_ko_number]
                    if eggnog_pathway:
                        for id in eggnog_pathway_ids:
                            for possible_path in possible_paths:
                                if id in possible_path:
                                    the_path = possible_path
                                    desc = the_path.split("||")[0].split("|-|")[1]
                                    neither = False
                                    break
                            else:
                                continue
                            master_dict[i]["annotations"].append(the_path)
                            annotation_tree(the_path, eggnog_ko_number, i, desc, fc, secondary, keggs_dict_secondary, keggs_dict_others)


                    elif eggnog_brite:
                        neither = True
                        for id in eggnog_brite_ids:
                            for possible_path in possible_paths:
                                if id in possible_path:
                                    the_path = possible_path
                                    desc = the_path.split("||")[0].split("|-|")[1]
                                    neither = False
                                    break
                            else:
                                continue
                            master_dict[i]["annotations"].append(the_path)
                            annotation_tree(the_path, eggnog_ko_number, i, desc, fc, secondary, keggs_dict_secondary, keggs_dict_others)
                    if neither:
                        the_path = possible_path
                        desc = the_path.split("||")[0].split("|-|")[1]
                        master_dict[i]["annotations"].append(the_path)
                        annotation_tree(the_path, eggnog_ko_number, i, desc, fc, secondary, keggs_dict_secondary, keggs_dict_others)

    with open(os.path.join(kegg_dir, "kegg_secondary_tree.json"), "w") as j:
        json.dump(keggs_dict_secondary, j, indent=4)
    with open(os.path.join(kegg_dir, "kegg_primary_tree.json"), "w") as j:
        json.dump(keggs_dict_others, j, indent=4)
    return master_dict


def generate_bgc_results(working_dir):
    parent_path = Path(working_dir).parent.absolute()
    global_files = os.path.join(parent_path, "global_files")
    if os.path.exists(global_files) != True:
        os.mkdir(global_files)
    with open(os.path.join(working_dir, "master_dict.json")) as f:
        master_dict = json.load(f)
    final_parsed_dir = os.path.join(working_dir, "final_results")
    if os.path.exists(final_parsed_dir) != True:
        os.mkdir(final_parsed_dir)
    final_bgc_dir = os.path.join(final_parsed_dir, "bgcs")
    if os.path.exists(final_bgc_dir) != True:
        os.mkdir(final_bgc_dir)
    bgcs = {}

    for i in master_dict:
        try:
            master_dict[i]["gene_name"] = i
            if master_dict[i]["bgc_prox"] != "false":
                if master_dict[i]["known_cluster"] == "N/A":
                    if master_dict[i]["region_number"] in bgcs:
                        bgcs[master_dict[i]["region_number"]]["genes"][master_dict[i]["protein_name"]] = master_dict[i]
                    else:
                        bgcs[master_dict[i]["region_number"]] = {"genes" : {master_dict[i]["protein_name"] : master_dict[i]}}
                else:
                    if master_dict[i]["known_cluster"] in bgcs:
                        bgcs[master_dict[i]["known_cluster"]]["genes"][master_dict[i]["protein_name"]] = master_dict[i]
                    else:
                        bgcs[master_dict[i]["known_cluster"]] = {"genes" : {master_dict[i]["protein_name"] : master_dict[i]}}
        except Exception as e:
            continue

    all_bgc_genes = {}
    for i in bgcs:
        bio_genes = []
        bio_add_genes = []
        mean_tpm1_bio = []
        mean_tpm2_bio = []
        mean_tpm1_bio_add = []
        mean_tpm2_bio_add = []
        fold_change_bio = []
        fold_change_add = []
        for gene in bgcs[i]["genes"]:
            try:
                tpm1 = bgcs[i]["genes"][gene]["tpm1"]
                tpm2 = bgcs[i]["genes"][gene]["tpm2"]
                fold_change = bgcs[i]["genes"][gene]["fold_change"]
                func = bgcs[i]["genes"][gene]["gene_function"].split()[0]
                padj = bgcs[i]["genes"][gene]["padj"]
                protein = bgcs[i]["genes"][gene]["protein_name"]
                gene_name = bgcs[i]["genes"][gene]["gene_name"]
            except:
                logging.warning("problematic genes in BGC %s with gene name %s"%(i, gene))
                continue
            if padj == "NA":
                continue
            if func == "biosynthetic-additional":
                mean_tpm1_bio_add.append(float(tpm1))
                mean_tpm2_bio_add.append(float(tpm2))
                bio_add_genes.append(gene_name)
                if float(padj) < 0.05:
                    fold_change_add.append(float(fold_change))
            if func == "biosynthetic":
                mean_tpm1_bio.append(float(tpm1))
                mean_tpm2_bio.append(float(tpm2))
                bio_genes.append(gene_name)
                if float(padj) < 0.05:
                    fold_change_bio.append(float(fold_change))
        if len(mean_tpm1_bio) != 0:
            mean_tpm1_bio = statistics.mean(mean_tpm1_bio)
        if len(mean_tpm2_bio) != 0:
            mean_tpm2_bio = statistics.mean(mean_tpm2_bio)
        if len(fold_change_bio) != 0:
            fold_change_bio = statistics.mean(fold_change_bio)
        if len(fold_change_add) != 0:
            fold_change_add = statistics.mean(fold_change_add)
        if len(mean_tpm1_bio_add) != 0:
            mean_tpm1_bio_add = statistics.mean(mean_tpm1_bio_add)
        if len(mean_tpm2_bio_add) != 0:
            mean_tpm2_bio_add = statistics.mean(mean_tpm2_bio_add)
        bgcs[i]["stats"] = {"fold_change_biosynthetic":fold_change_bio,
                            "fold_change_biosynthetic-additional":fold_change_add, "mean_tpm1_biosynthetic": mean_tpm1_bio
                            , "mean_tpm2_biosynthetic": mean_tpm2_bio, "mean_tpm1_biosynthetic-additional": mean_tpm1_bio_add,
                            "mean_tpm2_biosynthetic-additional": mean_tpm2_bio_add, "bio_add_genes": bio_add_genes, "bio_genes":bio_genes}
        genes = bgcs[i].pop("genes")
        bgc_gene_list = []
        gene_counter = 0
        for new_gene in genes:
            gene_counter += 1
            if gene_counter == 1:
                bgc_started = genes[new_gene]["gene_start"]
            if gene_counter == len(genes) :
                bgc_ended = genes[new_gene]["gene_end"]
        try:
            all_bgc_genes[i] = {"bgc_start" : int(bgc_started), "bgc_end" : int(bgc_ended), "genes" : bgc_gene_list}
        except:
            "somethings wrong with bgcs, line 434 final results"
        for new_gene in genes:
            bgc_gene_list.append(genes[new_gene]["gene_name"])
        bgcs[i]["prox_genes"] = bgc_gene_list
        json.dump(bgcs[i], open(os.path.join(final_bgc_dir, i.replace(" ", "_").replace("/","-") + ".json"), "w"), indent=4)
    if os.path.exists(os.path.join(global_files, "all_bgc_genes.json")) != True:
        json.dump(all_bgc_genes,
                  open(os.path.join(global_files, "all_bgc_genes_global.json"), "w"),
                  indent=4)
    with open(os.path.join(working_dir, "bgcs.pickle"), "wb") as bgcs_pickle:
        pickle.dump(bgcs, bgcs_pickle)


def string_int_conversions(working_dir, conditions):
    with open(os.path.join(working_dir, "master_dict.json")) as f:
        master_dict = json.load(f)
    for gene in master_dict:
        try:
            master_dict[gene]["tpm1||%s"%conditions[0]] = master_dict[gene].pop["tpm1"]
        except:
            print("no tpm1")
        try:
            master_dict[gene]["tpm2||%s"%conditions[1]] = master_dict[gene].pop["tpm2"]
        except:
            print("no tpm2")
        try:
            master_dict[gene]["gene_start"] = int(master_dict[gene]["gene_start"])
        except:
            continue
        try:
            master_dict[gene]["gene_end"] = int(master_dict[gene]["gene_end"])
        except:
            continue
        try:
            master_dict[gene]["tpm1"] = float(master_dict[gene]["tpm1"])
        except:
            print("no tpm1")
        try:
            master_dict[gene]["tpm2"] = float(master_dict[gene]["tpm2"])
        except:
            print("no tpm2")
        try:
            master_dict[gene]["fold_change"] = float(master_dict[gene]["fold_change"])
        except:
            print("no fc")
        try:
            master_dict[gene]["padj"] = float(master_dict[gene]["padj"])
        except:
            print("no padj")
    return master_dict

def add_to_bgcs(working_dir, rev_path, conc_path):
    with open(rev_path) as f:
        revs = json.load(f)
    final_parsed_dir = os.path.join(working_dir, "final_results")
    if os.path.exists(final_parsed_dir) != True:
        os.mkdir(final_parsed_dir)
    final_bgc_dir = os.path.join(final_parsed_dir, "bgcs")
    with open(conc_path) as g:
        conc = json.load(g)
    with open(os.path.join(working_dir, "bgcs.pickle"), "rb") as bg:
        bgcs = pickle.load(bg)
    for i in bgcs:
        if "concordant_genes" not in bgcs[i]:
            bgcs[i]["concordant_genes"] = []
        if "reverse_genes" not in bgcs[i]:
            bgcs[i]["reverse_genes"] = []
        for y in conc:
            if y.split("||")[0] == i:
                bgcs[i]["concordant_genes"].append(y.split("||")[1])
        for x in revs:
            if x.split("||")[0] == i:
                bgcs[i]["reverse_genes"].append(x.split("||")[1])
        json.dump(bgcs[i], open(os.path.join(final_bgc_dir, i.replace(" ", "_").replace("/","-") + ".json"), "w"), indent=4)
    print("end")

def adapt_bgcs(working_dir):
    experiment_files = os.path.join(working_dir, "experiment_specific_files")
    if os.path.exists(experiment_files) != True:
        os.mkdir(experiment_files)
    with open(os.path.join(working_dir, "bgcs.pickle"), "rb") as bg:
        bgcs = pickle.load(bg)
    final_bgc_dir = os.path.join(working_dir, "final_results", "bgcs")
    all_bgcs = {}
    bgc_list = os.listdir(final_bgc_dir)
    for bgc in bgcs:
        if os.path.exists(os.path.join(final_bgc_dir, bgc.replace(" ", "_").replace("/","-")) + ".json"):
            with open(os.path.join(final_bgc_dir, bgc.replace(" ", "_").replace("/","-")) + ".json") as f:
                bgc_dict = json.load(f)
        else:
            print("no")
        try:
            new_stats_dict = bgc_dict["stats"]
        except:
            print(final_bgc_dir, bgc)
        del new_stats_dict["bio_add_genes"]
        del new_stats_dict["bio_genes"]
        bgc = bgc.replace(".json", "")
        all_bgcs[bgc] = bgc_dict["stats"]
        all_bgcs[bgc]["concordant_genes"] = bgc_dict["concordant_genes"]
        all_bgcs[bgc]["reverse_genes"] = bgc_dict["reverse_genes"]
    json.dump(all_bgcs, open(os.path.join(experiment_files, "all_bgcs_experimental.json"), "w"), indent=4)

def adapt_master_dict(working_dir):
    parent_path = Path(working_dir).parent.absolute()
    global_files = os.path.join(parent_path, "global_files")
    experiment_files = os.path.join(working_dir, "experiment_specific_files")
    if os.path.exists(experiment_files) != True:
        os.mkdir(experiment_files)
    if os.path.exists(global_files) != True:
        os.mkdir(global_files)

    experiment_specific = {}
    annot_gene = {}
    kegg_secondary = {}
    kegg_primary = {}
    parent_path = Path(working_dir).parent.absolute()
    with open(os.path.join(working_dir, "master_dict.json")) as f:
        master_dict = json.load(f)
    for gene in master_dict:
        if "product" in master_dict[gene] and "annotations" in master_dict[gene]:
            if "transport" in master_dict[gene]["product"] or "export" in master_dict[gene]["product"] or "secret" in master_dict[gene]["product"]:
                master_dict[gene]["transport"] = "true"
            else:
                if master_dict[gene]["annotations"] != "None" or master_dict[gene]["annotations"] != "N/A":
                    for anno in master_dict[gene]["annotations"]:
                        if "transport" in anno or "export" in anno or "secret" in anno:
                            master_dict[gene]["transport"] = "true"
                    else:
                        master_dict[gene]["transport"] = "false"
                else:
                    master_dict[gene]["transport"] = "false"
        if "secondary" in master_dict[gene]:
            if master_dict[gene]["secondary"] == "true":
                if master_dict[gene]["annotations"] != []:
                    for annot in master_dict[gene]["annotations"]:
                        knum = annot.split("|-|")[0]
                        if knum not in kegg_secondary:
                            kegg_secondary[knum] = [gene]
                        else:
                            if gene not in kegg_secondary[knum]:
                                kegg_secondary[knum].append(gene)
            if master_dict[gene]["secondary"] != "true":
                if master_dict[gene]["annotations"] != [] and master_dict[gene]["annotations"] != "N/A" and master_dict[gene]["annotations"] != "None":
                    for annot in master_dict[gene]["annotations"]:
                        knum = annot.split("|-|")[0]
                        if knum not in kegg_primary:
                            kegg_primary[knum] = [gene]
                        else:
                            if gene not in kegg_primary[knum]:
                                kegg_primary[knum].append(gene)
        experiment_specific[gene] = {}
        annot_gene[gene] = {}
        for spec in master_dict[gene]:
            if "tpm1" in spec or "tpm2" in spec or "fold_change" in spec or "padj" in spec:
                experiment_specific[gene][spec] = master_dict[gene][spec]
            else:
                if spec == "gene_function":
                    annot_gene[gene][spec] = master_dict[gene][spec]
                    if master_dict[gene][spec].startswith("biosynthetic "):
                        annot_gene[gene]["is_bio_core"] = "true"
                    else:
                        annot_gene[gene]["is_bio_core"] = "false"
                    if master_dict[gene][spec].startswith("biosynthetic-additional "):
                        annot_gene[gene]["is_bio_additional"] = "true"
                    else:
                        annot_gene[gene]["is_bio_additional"] = "false"
                else:
                    annot_gene[gene][spec] = master_dict[gene][spec]

    if os.path.exists(os.path.join(global_files, "annotation_gene_dict.json")) != True:
        json.dump(annot_gene, open(os.path.join(global_files, "annotation_gene_dict.json"), "w"), indent=4)

    if os.path.exists(os.path.join(global_files, "kegg_secondary_map.json")) != True:
        json.dump(kegg_secondary, open(os.path.join(global_files, "kegg_secondary_map.json"), "w"), indent=4)

    if os.path.exists(os.path.join(global_files, "kegg_primary_map.json")) != True:
        json.dump(kegg_primary, open(os.path.join(global_files, "kegg_primary_map.json"), "w"), indent=4)

    json.dump(experiment_specific, open(os.path.join(experiment_files, "experiment_gene_dict.json"), "w"), indent=4)




def generate_housekeeping_tpm_averages(working_dir, conditions):
    parent_path = Path(working_dir).parent.absolute()
    global_files = os.path.join(parent_path, "global_files")
    if os.path.exists(global_files) != True:
        os.mkdir(global_files)
    cur_dir = str(pathlib.Path(__file__).parent.parent.resolve())
    tigr_funct = os.path.join(cur_dir, "static/tigr_to_function_dict.pickle")

    with open(tigr_funct, 'rb') as handle:
        tigr_to_function_dict = pickle.load(handle)
    parent_path = Path(working_dir).parent.absolute()
    cond1 = conditions[0]
    cond2 = conditions[1]
    tpms_json_path = os.path.join(parent_path, "combined", "tpms.json")
    with open(os.path.join(working_dir, "master_dict.json")) as f:
        master_dict = json.load(f)
    tpm_dict = {}
    final_parsed_dir = os.path.join(working_dir, "final_results")
    if os.path.exists(final_parsed_dir) != True:
        os.mkdir(final_parsed_dir)
    with open(os.path.join(working_dir, "bgcs.pickle"), "rb") as bg:
        bgcs = pickle.load(bg)
    mean_tpm1 = []
    mean_tpm2 = []
    mean_tpm1_non_hous = []
    mean_tpm2_non_hous = []
    mean_tpm1_all = []
    mean_tpm2_all = []
    tigr_tpms = {}
    for gene in master_dict:
        try:
            mean_tpm1_all.append(float(master_dict[gene]["tpm1"]))
            mean_tpm2_all.append(float(master_dict[gene]["tpm2"]))
        except:
            print("no tpm or problematic gene")
        if "housekeeping" in master_dict[gene]:
            if master_dict[gene]["housekeeping"] == "false" or master_dict[gene]["housekeeping"] == "N/A" or master_dict[gene]["housekeeping"] == "NA":
                try:
                    mean_tpm1_non_hous.append(float(master_dict[gene]["tpm1"]))
                    mean_tpm2_non_hous.append(float(master_dict[gene]["tpm2"]))
                except:
                    print("no tpm or problematic gene")
            else:
                try:
                    tigr = master_dict[gene]["housekeeping"]
                    if tigr in tigr_to_function_dict:
                        category = tigr_to_function_dict[tigr]
                        if category + "_tpm1" in tigr_tpms:
                            tigr_tpms[category + "_tpm1"].append(float(master_dict[gene]["tpm1"]))
                            tigr_tpms[category + "_tpm2"].append(float(master_dict[gene]["tpm2"]))
                        else:
                            tigr_tpms[category + "_tpm1"] = [float(master_dict[gene]["tpm1"])]
                            tigr_tpms[category + "_tpm2"]= [float(master_dict[gene]["tpm2"])]
                    mean_tpm1.append(float(master_dict[gene]["tpm1"]))
                    mean_tpm2.append(float(master_dict[gene]["tpm2"]))
                except:
                    print("problem with tpm value gene: %s"%gene)
    first = statistics.mean(mean_tpm1)
    second = statistics.mean(mean_tpm2)
    first_all = statistics.mean(mean_tpm1_all)
    second_all = statistics.mean(mean_tpm2_all)
    first_non_hous = statistics.mean(mean_tpm1_non_hous)
    second_non_hous = statistics.mean(mean_tpm2_non_hous)
    tpm_dict[cond1] = {"mean_housekeeping_tpm" : first, "mean_non_hous_tpm" : first_non_hous,  "mean_all_tpm" : first_all, "BGCS above" : {}, "BGCS below" : {}}
    tpm_dict[cond2] = {"mean_housekeeping_tpm" : second, "mean_non_hous_tpm" : second_non_hous, "mean_all_tpm" : second_all, "BGCS above" : {}, "BGCS below" : {}}
    for tigr_cat_tpm in tigr_tpms:
        if "_tpm1" in tigr_cat_tpm:
            tpm_dict[cond1][tigr_cat_tpm.split("_tpm")[0]] = statistics.mean(tigr_tpms[tigr_cat_tpm])
        if "_tpm2" in tigr_cat_tpm:
            tpm_dict[cond2][tigr_cat_tpm.split("_tpm")[0]] = statistics.mean(tigr_tpms[tigr_cat_tpm])
    print(str(statistics.mean(mean_tpm1)))
    print(str(statistics.mean(mean_tpm2)))
    for bgc in bgcs:
        x = 0
        for _ in conditions:
            x += 1
            if x == 1:
                current_cond = cond1
            else:
                current_cond = cond2
            if bgcs[bgc]["stats"]["mean_tpm%s_biosynthetic" % str(x)] == []:
                continue
            if bgcs[bgc]["stats"]["mean_tpm%s_biosynthetic"%str(x)] >=  float(tpm_dict[current_cond]["mean_housekeeping_tpm"]):
                tpm_dict[current_cond]["BGCS above"][bgc] = bgcs[bgc]["stats"]["mean_tpm%s_biosynthetic"%str(x)]
            else:
                tpm_dict[current_cond]["BGCS below"][bgc] = bgcs[bgc]["stats"]["mean_tpm%s_biosynthetic"%str(x)]
    if os.path.exists(os.path.join(parent_path, "combined")) != True:
        os.mkdir(os.path.join(parent_path, "combined"))
    if os.path.exists(tpms_json_path):
        with open(tpms_json_path) as f:
            old_tpms = json.load(f)
        for cond in old_tpms:
            if cond in tpm_dict:
                tpm_dict[cond]["BGCS below"] = dict(sorted(tpm_dict[cond]["BGCS below"].items(), key=lambda item: (item[1]), reverse = True))
                tpm_dict[cond]["BGCS above"] = dict(sorted(tpm_dict[cond]["BGCS above"].items(), key=lambda item: (item[1]), reverse = True))
                continue
            else:
                tpm_dict[cond] = old_tpms[cond]
            tpm_dict[cond]["BGCS below"] = dict(sorted(tpm_dict[cond]["BGCS below"].items(), key=lambda item: (item[1]), reverse = True))
            tpm_dict[cond]["BGCS above"] = dict(sorted(tpm_dict[cond]["BGCS above"].items(), key=lambda item: (item[1]), reverse = True))
        tpm_dict = dict(sorted(tpm_dict.items(), key=lambda item: (len(item[1]["BGCS above"])), reverse = True))
        with open(os.path.join(working_dir, tpms_json_path), "w") as j:
            json.dump(tpm_dict, j, indent=4)
    else:
        for cond in tpm_dict:
            tpm_dict[cond]["BGCS below"] = dict(sorted(tpm_dict[cond]["BGCS below"].items(), key=lambda item: (item[1]), reverse = True))
            tpm_dict[cond]["BGCS above"] = dict(sorted(tpm_dict[cond]["BGCS above"].items(), key=lambda item: (item[1]), reverse = True))
        tpm_dict = dict(sorted(tpm_dict.items(), key=lambda item: (len(item[1]["BGCS above"])), reverse = True))
        if "phantom_cond" in tpm_dict:
            del tpm_dict["phantom_cond"]
        with open(tpms_json_path, "w") as j:
            json.dump(tpm_dict, j, indent=4)
    if os.path.exists(os.path.join(global_files,"tpms.json")):
        os.remove(os.path.join(global_files,"tpms.json"))
    copy2(tpms_json_path, global_files)


def adapt_kegg_json(kegg_json):
    hierarchy = []
    geneMap = {}
    for level1 in kegg_json:
        hierarchyLevel2 = []
        leavesLevel2 = []
        for level2 in kegg_json[level1]:
            if level2 != "id" and level2 != "count":
                hierarchyLevel3 = []
                leavesLevel3 = []
                for level3 in kegg_json[level1][level2]:
                    if level3 != "child_id" and level3 != "count":
                        hierarchyLevel4 = []
                        leavesLevel4 = []
                        for level4 in kegg_json[level1][level2][level3]:
                            if level4 != "grand_child_id" and level4 != "count":
                                leavesLevel4.append(level4)
                                hierarchyLevel4.append(
                                    {"label": kegg_json[level1][level2][level3][level4]["description"],
                                     "value": level4, "leaves": [level4]})
                                genes = []
                                for gene in kegg_json[level1][level2][level3][level4]['genes']:
                                    genes.append(gene.split("|")[0])
                                geneMap[level4] = genes
                        leavesLevel3 = leavesLevel3 + leavesLevel4
                        hierarchyLevel3.append(
                            {"label": level3, "value": kegg_json[level1][level2][level3]["grand_child_id"],
                             "children": hierarchyLevel4, "leaves": leavesLevel4})
                leavesLevel2 = leavesLevel2 + leavesLevel3
                hierarchyLevel2.append(
                    {"label": level2, "value": kegg_json[level1][level2]["child_id"], "children": hierarchyLevel3,
                     "leaves": leavesLevel3})
        hierarchy.append(
            {"label": level1, "value": kegg_json[level1]["id"], "children": hierarchyLevel2, "leaves": leavesLevel2})
    return {"hierarchy": hierarchy, "geneMap": geneMap}


def write_adapted_kegg(working_dir):
    parent_path = Path(working_dir).parent.absolute()
    global_files = os.path.join(parent_path, "global_files")
    keg_path = os.path.join(working_dir, "final_results", "kegg", "kegg_secondary_tree.json")
    with open(keg_path) as f:
        keg_dict = json.load(f)
    new_kegg = adapt_kegg_json(keg_dict)
    with open(os.path.join(global_files, "kegg_secondary_tree.json"), "w") as j:
        json.dump(new_kegg['hierarchy'], j, indent=4)
    keg_path = os.path.join(working_dir, "final_results", "kegg", "kegg_primary_tree.json")
    with open(keg_path) as f:
        keg_dict = json.load(f)
    new_kegg = adapt_kegg_json(keg_dict)
    with open(os.path.join(global_files, "kegg_primary_tree.json"), "w") as j:
        json.dump(new_kegg['hierarchy'], j, indent=4)


if __name__ == '__main__':
    print("main")