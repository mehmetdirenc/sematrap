#  Copyright (C) 2021,2022 Mehmet Direnc Mungan
#  University of Tuebingen
#  Interfaculty Institute of Microbiology and Infection Medicine
#  Lab of Nadine Ziemert, Div. of Microbiology/Biotechnology
#  Funding by the German Centre for Infection Research (DZIF)
#  #
#  This file is part of SeMa-Trap
#  SeMa-Trap is free software. you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  #
#  #This program is distributed in the hope that it will be useful,
#   #but WITHOUT ANY WARRANTY; without even the implied warranty of
#   #MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   #GNU General Public License for more details.
#
#  License: You should have received a copy of the GNU General Public License v3 with SeMa-Trap
#  A copy of the GPLv3 can also be found at: <http://www.gnu.org/licenses/>.

import argparse, os
from sematrap.backend.parse_antismash_results import *
from sematrap.backend.mapping_analysis import *
from sematrap.backend.hmm_searches import *
from sematrap.backend.download_fastq import *
from sematrap.backend.compare_bgc_trends import *
from sematrap.backend.final_results import *
from sematrap.backend.kegg_eggnog import *
import time
from shutil import copy2

def run_single(working_dir, genome_name,
             cores, paired_or_single, gbk_file,
             regulator_hmms, resistance_hmm_path, rscript_path,
            sra_input, fastq_files, ncbi_annotation, read_source, user_cluster, refset, refset_path, annotation, multiple, search_type, egg_data_dir, uploads_folder):
    working_dir_list = working_dir.split(",")
    if sra_input != None:
        sra_input = sra_input.split(",")
    else:
        sra_input = []
        for y in range(0, len(working_dir_list)):
            sra_input.append(None)
    if fastq_files != None:
        fastq_files = fastq_files.split(",")
    else:
        fastq_files = []
        for y in range(0, len(working_dir_list)):
            fastq_files.append(None)
    logging.info("working_dir_list: " + str(working_dir_list))
    logging.info("multiple: " + str(multiple))
    if len(working_dir_list) != 1:
        return
    else:
        sra_input = sra_input[0]
        fastq_files = fastq_files[0]
        working_dir = working_dir_list[0]
    if os.path.exists(working_dir) != True:
        os.mkdir(working_dir)
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)
    log_path = os.path.join(working_dir, "workflow.log")
    logging.basicConfig(filename=log_path, filemode='w', level=logging.DEBUG, format='%(asctime)s.%(msecs)03d %(levelname)s %(module)s - %(funcName)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    initial_folder = os.path.basename(Path(gbk_file).parent.absolute())
    logging.info("initial folder: %s"%initial_folder)
    if annotation == True or annotation == "True":
        gtf_path, fna_path, gbff_path, protein_filename = download_gtf_fna_gbk_file(gbk_file, working_dir)
        antismash_gbk_result = runantismash(working_dir, gbff_path, cores, multiple)
    else:
        if os.path.exists(gbk_file) != True:
            logging.error("Please provide a local file if its not annotated")
            print("Please provide a local file if its not annotated")
            import sys
            sys.exit()
        antismash_gbk_result = runantismash(working_dir, gbk_file, cores, multiple)
        gtf_path, fna_path, gbff_path, protein_filename, antismash_gbk_result = convert_gbk(antismash_gbk_result, working_dir)

    fastp_path="fastp"
    index_output_path = os.path.join(working_dir, "hisat_index")
    mapping_results =  os.path.join(working_dir, "hisat2_results")
    if os.path.exists(working_dir) != True:
        os.mkdir(working_dir)
    logging.info("downloading sra")
    if fastq_files != None:
        fastq_files = fastq_files
        input_order = []
        for i in open(sra_input, "r"):
            if i.split("\t")[-1].strip("\n") not in input_order:
                input_order.append(i.split("\t")[-1].strip("\n"))
    else:
        sra_input = sra_input
        ##TODO UNCOMMENT THIS##
        if search_type != "bam":
            fastq_files = download_fastq(sra_input, working_dir, paired_or_single)
        fastq_files = os.path.join(working_dir, "fastq_files")
        input_order = []
        for i in open(sra_input, "r"):
            if i.split("\t")[-1].strip("\n") not in input_order:
                input_order.append(i.split("\t")[-1].strip("\n"))
    logging.info(input_order)
    if search_type != "single":
        con1 = input_order[0]
        con2 = input_order[1]
    else:
        con1 = input_order[0]
        con2 = "phantom_cond"
    logging.info("From %s to %s"%(con1, con2))
    conditions = [con1, con2]
    con_dict = {working_dir : {"con1" : con1, "con2" : con2}}
    master_dict = master_gene_dict(gtf_path)
    hmm_result_reg = run_regulators_hmm(working_dir, cores, regulator_hmms, protein_filename)
    regulators_list, regulators_dict = parse_regulators_hmm_result(hmm_result_reg)
    hmm_result_res = run_resistance_hmm(working_dir, cores, resistance_hmm_path, protein_filename)
    resistance_list, resistance_dict = parse_resistance_hmm_result(hmm_result_res)
    if refset != None:
        housekeeping_res = housekeeping_gene_search(working_dir, refset, refset_path, protein_filename, cores)
        housekeeping_list, housekeeping_dict = parse_housekeeping_hmm_result(housekeeping_res)
    else:
        housekeeping_list, housekeeping_dict = [], {}
    run_emapper(working_dir, genome_name, protein_filename, multiple, cores, egg_data_dir)
    master_dict = check_pos(antismash_gbk_result, working_dir,
                            regulators_list, regulators_dict,
                            resistance_list, resistance_dict,
                            master_dict, user_cluster, housekeeping_list, housekeeping_dict)
    logging.info("indexing")
    if search_type != "bam":
        hisat_build_index(fna_path, index_output_path, genome_name)
    logging.info("trimming")
    if search_type != "bam":
        data_dir_dict = trimming(fastp_path, paired_or_single, fastq_files, input_order, read_source)
    logging.info("mapping")
    index_basename = os.path.join(index_output_path, genome_name)
    if search_type != "bam":
        bams = samtools_mapping(data_dir_dict, index_basename, mapping_results, cores, paired_or_single)
        logging.info("data_dir_dict: " + str(data_dir_dict))
    elif search_type == "bam":
        if os.path.exists(mapping_results) != True:
            os.mkdir(mapping_results)
        bams = {}
        for i in open(sra_input, "r"):
            bam_cond = i.split("\t")[-1].strip()
            bam_file = i.split("\t")[0].strip()
            bam_path_to_copy = os.path.join(uploads_folder, initial_folder, bam_file)
            bam_path_to_copy = check_bam_header(bam_path_to_copy, gtf_path, working_dir)
            bam_file = "new_" + i.split("\t")[0].strip()
            copy2(bam_path_to_copy, mapping_results)
            if bam_cond not in bams:
                bams[bam_cond] = {bam_file : os.path.join(mapping_results, bam_file)}
            else:
                bams[bam_cond][bam_file] = os.path.join(mapping_results, bam_file)
    logging.info("bams: " + str(bams))
    new_gtf_path = convert_gff_to_gtf(gtf_path)
    features_path, bams = run_feature_count(cores, new_gtf_path, working_dir, bams, paired_or_single, master_dict, search_type)
    time.sleep(2)
    if search_type != "single":
        deseq_result = run_deseq2(rscript_path, features_path, bams, working_dir, con1, con2)
        master_dict = search_annotation(working_dir, gtf_path, master_dict)
    else:
        for single_gene in master_dict:
            master_dict[single_gene]["fold_change"] = "0"
            master_dict[single_gene]["padj"] = "0"
            master_dict[single_gene]["tpm2"] = "0"
            master_dict[single_gene]["tpm2||phantom_cond"] = "0"
    json.dump(master_dict, open(os.path.join(working_dir, "master_dict.json"), 'w'), indent=4)
    master_dict = generate_kegg_groups(working_dir)
    json.dump(master_dict, open(os.path.join(working_dir, "master_dict.json"), 'w'), indent=4)
    master_dict = string_int_conversions(working_dir, conditions)
    json.dump(master_dict, open(os.path.join(working_dir, "master_dict.json"), 'w'), indent=4)
    generate_bgc_results(working_dir)
    generate_regulators(working_dir)
    generate_resistance_genes(working_dir)
    single_conc_path, single_rev_path = comparison([working_dir], 1.0, -1.0)
    add_to_bgcs(working_dir, single_rev_path, single_conc_path)
    adapt_bgcs(working_dir)
    adapt_master_dict(working_dir)
    generate_housekeeping_tpm_averages(working_dir, conditions)
    write_adapted_kegg(working_dir)
    logging.info("THE END")
    asdir = os.path.join(working_dir, "antismash_output")
    parent_dir = Path(working_dir).parent.absolute()
    if os.path.exists(asdir) and os.path.exists(os.path.join(parent_dir, "antismash_output")) != True:
        shutil.move(asdir, parent_dir)
    egdir = os.path.join(working_dir, "eggnog_results")
    parent_dir = Path(working_dir).parent.absolute()
    if os.path.exists(egdir) and os.path.exists(os.path.join(parent_dir, "eggnog_results")) != True:
        shutil.move(egdir, parent_dir)
    print("THE END")
    return con_dict

def run_multiple(working_dir, genome_name,
             cores, paired_or_single, gbk_file,
             regulator_hmms, resistance_hmm_path, rscript_path,
            sra_input, fastq_files, ncbi_annotation, read_source, user_cluster, refset, refset_path, annotation, multiple, search_type, egg_data_dir, uploads_folder):
    working_dir_list = working_dir.split(",")
    if sra_input != None:
        sra_input = sra_input.split(",")
    else:
        sra_input = []
        for y in range(0, len(working_dir_list)):
            sra_input.append(None)
    if fastq_files != None:
        fastq_files = fastq_files.split(",")
    else:
        fastq_files = []
        for y in range(0, len(working_dir_list)):
            fastq_files.append(None)
    if len(working_dir_list) < 1:
        return
    else:
        all_conds_dict = {}
        for i in range(0, len(working_dir_list)):
            con_dict = run_single(working_dir_list[i], genome_name,
             cores, paired_or_single, gbk_file,
             regulator_hmms, resistance_hmm_path, rscript_path,
            sra_input[i], fastq_files[i], ncbi_annotation, read_source, user_cluster, refset, refset_path, annotation, multiple, search_type, egg_data_dir, uploads_folder)
            for con in con_dict:
                all_conds_dict[con] = con_dict[con]
    parent_dir = Path(working_dir_list[0]).parent.absolute()
    json.dump(all_conds_dict, open(os.path.join(parent_dir, "working_directory_conditions_dict.json"), 'w'), indent=4)

    comparison(working_dir_list, 1.0, -1.0)
    return "run_multiple_end"





if __name__ == '__main__':
    dir_path = Path(os.path.realpath(__file__)).parent.parent.absolute()
    hmm_dir = os.path.join(dir_path, "static", "hmms")
    logging.info(dir_path)
    parser = argparse.ArgumentParser(description="""Transciptomics Pipeline for BGC Production Increase""")
    parser.add_argument("-fastq_folder_path", help="Either fastq folder or SRA input file is required", default=None)
    parser.add_argument("-sra_input_file", help="Either fastq folder or SRA input file is required", default=None)
    parser.add_argument("-working_dir", help="Directory of output files", default=None)
    parser.add_argument("-genome", help="Either path of a local gbk file or assembly id e.g. GCF_000016425.1")
    parser.add_argument("-name",  help="Genome name", default="genome_1")
    parser.add_argument("-cpu", help="Turn on Multi processing set # Cpus (default: Off, 1)", type=int, default=1)
    parser.add_argument("-reg", help="HMM file path consisting of regulators", default=os.path.join(hmm_dir, "all_regulators_pfam.hmm"))
    parser.add_argument("-res", help="User supplied core models. hmm file", default=os.path.join(hmm_dir, "knownresistance.hmm"))
    parser.add_argument("-exp_design", help="Experimental design: pe for paired end s for single end", default="pe")
    parser.add_argument("-rscript_path", help="Rscript path from usr folder (conda version gave errors)", default="Rscript")
    parser.add_argument("-ncbi_annotation", help="Is the genome file annotated in ncbi", default=None)
    parser.add_argument("-read_source", help="Either ena or sra is supported", default="sra")
    parser.add_argument("-user_cluster", help="provide original clusters", default=None)
    parser.add_argument("-refset", help="provide taxonomic info e.g. actinobacteria", default=None)
    parser.add_argument("-refset_path", help="folder path of all reference sets", default=None)
    parser.add_argument("-annotation", help="Is the genome annotated in ncbi and has accesion number", default=True)
    parser.add_argument("-multiple", help="Define if there are multiple experiments from the same organism", default=False)
    parser.add_argument("-search_type", help="Define the analysis type based on the input. Options : bam,dual,single", default="dual")
    parser.add_argument("-egg_data_dir", help="location of eggnog data directory", default=None)
    parser.add_argument("-uploads_folder", help="Define local uploads folder path", default=None)
    args = parser.parse_args()
    print(args)
    logging.info(args)
    run_single(working_dir=args.working_dir, genome_name=args.name,
            cores=args.cpu, paired_or_single=args.exp_design, gbk_file=args.genome, regulator_hmms=args.reg, sra_input=args.sra_input_file
            , resistance_hmm_path=args.res, rscript_path=args.rscript_path,
            fastq_files=args.fastq_folder_path, ncbi_annotation = args.ncbi_annotation, read_source=args.read_source,
            user_cluster = args.user_cluster, refset = args.refset, refset_path=args.refset_path, annotation=args.annotation
                 , multiple=args.multiple, search_type=args.search_type, egg_data_dir=args.egg_data_dir, uploads_folder=args.uploads_folder)
    run_multiple(working_dir=args.working_dir, genome_name=args.name,
            cores=args.cpu, paired_or_single=args.exp_design, gbk_file=args.genome, regulator_hmms=args.reg, sra_input=args.sra_input_file
            , resistance_hmm_path=args.res, rscript_path=args.rscript_path,
            fastq_files=args.fastq_folder_path, ncbi_annotation = args.ncbi_annotation, read_source=args.read_source,
            user_cluster = args.user_cluster, refset = args.refset, refset_path=args.refset_path, annotation=args.annotation
                 , multiple=args.multiple, search_type=args.search_type, egg_data_dir=args.egg_data_dir, uploads_folder=args.uploads_folder)

    # working_dir = ["/media/ecoli/2gb/transcriptomics/amy_japonicum/zn24h_wt_vs_zurko/", "/media/ecoli/2gb/transcriptomics/amy_japonicum/24_control_wt_vs_zur_ko","/media/ecoli/2gb/transcriptomics/amy_japonicum/24zurko_zn_vs_control/", "/media/ecoli/2gb/transcriptomics/amy_japonicum/zn60_wt_vs_zurko"]
    # fastq_folder_path = ["/media/ecoli/2gb/transcriptomics/amy_japonicum/zn24h_wt_vs_zurko/fastq_files", "/media/ecoli/2gb/transcriptomics/amy_japonicum/24_control_wt_vs_zur_ko/fastq_files","/media/ecoli/2gb/transcriptomics/amy_japonicum/24zurko_zn_vs_control/fastq_files", "/media/ecoli/2gb/transcriptomics/amy_japonicum/zn60_wt_vs_zurko/fastq_files"]

    # working_dir_list = ["/media/ecoli/2gb/transcriptomics/sc_a2_working_dir/isme_coculture_xanthus/t4_pure_vs_cocult"
    #                     ,"/media/ecoli/2gb/transcriptomics/sc_a2_working_dir/isme_coculture_xanthus/t3_pure_vs_cocult"
    #                     ,"/media/ecoli/2gb/transcriptomics/sc_a2_working_dir/isme_coculture_xanthus/t2_pure_vs_cocult"
    #                     ,"/media/ecoli/2gb/transcriptomics/sc_a2_working_dir/isme_coculture_xanthus/t1_pure_vs_cocult"
    #                     ,"/media/ecoli/2gb/transcriptomics/sc_a2_working_dir/isme_coculture_xanthus/ctt_vs_r5solid"
    #                     ,"/media/ecoli/2gb/transcriptomics/sc_a2_working_dir/isme_coculture_xanthus/ctt_vs_350"]
    # fastq_files = ["/media/ecoli/2gb/transcriptomics/sc_a2_working_dir/isme_coculture_xanthus/t4_pure_vs_cocult/fastq_files"
    #                     ,"/media/ecoli/2gb/transcriptomics/sc_a2_working_dir/isme_coculture_xanthus/t3_pure_vs_cocult/fastq_files"
    #                     ,"/media/ecoli/2gb/transcriptomics/sc_a2_working_dir/isme_coculture_xanthus/t2_pure_vs_cocult/fastq_files"
    #                     ,"/media/ecoli/2gb/transcriptomics/sc_a2_working_dir/isme_coculture_xanthus/t1_pure_vs_cocult/fastq_files"
    #                     ,"/media/ecoli/2gb/transcriptomics/sc_a2_working_dir/isme_coculture_xanthus/ctt_vs_r5solid/fastq_files"
    #                     ,"/media/ecoli/2gb/transcriptomics/sc_a2_working_dir/isme_coculture_xanthus/ctt_vs_350/fastq_files"]

    # paired_or_single = "s"
    # fastq_files = "/Users/direnc/transcriptomics/Streptomycescoelicolor_timeseries_RNAseqdata/bamfiles/fastq"
    # working_dir = "/Users/direnc/transcriptomics/sc_a2_working_dir/kays"
    # # paired_or_single = "pe"
    # # fastq_files = "/media/ecoli/2gb/transcriptomics_1tb/ifi/Project_IIT-KA-GFB-2015-036/Sample_Abalhamycina_C/Abalhamycina_C_CAGATC_L001_R1_001.fastq,/media/ecoli/2gb/transcriptomics_1tb/ifi/Project_IIT-KA-GFB-2015-036/Sample_Abalhamycina_C/Abalhamycina_C_CAGATC_L001_R2_001.fastq"
    # # fastq_files = "/media/ecoli/2gb/transcriptomics/sc_a2_working_dir/wild_fastq/wild_type_SRR8146246.fastq"
    # # fastq_files = "/media/ecoli/2gb/transcriptomics/sc_a2_working_dir/pl4_fastq_paired/" \
    # #               "PL4_GGCTAC_L002_R1_001.fastq,/media/ecoli/2gb/transcriptomics/sc_a2_working" \
    # #               "_dir/pl4_fastq_paired/PL4_GGCTAC_L002_R2_001.fastq"
    # # working_dir = "/media/ecoli/2gb/transcriptomics/sc_a2_working_dir/wild"
    # regulator_hmms = "/media/ecoli/2gb/transcriptomics/luxr_pfam.hmm"
    # aspath = "/media/ecoli/1tb/antismash_5/antismash_final_version/run_antismash.py"
    # paired_filename = "sco_pl4"
    # gbk_file = "/media/ecoli/2gb/transcriptomics/sc_a2_working_dir/reference_genome/GCF_000203835.1_ASM20383v1_genomic.fna"
    # index_output_path = os.path.join(working_dir, "hisat_index")
    # genome_name = "sca32"
    # mapping_results =  os.path.join(working_dir, "hisat2_results")
    # cores = "10"
    #

    ################## configurations stropcnb440 ####################
# -fastq_folder_path
# /media/ecoli/2gb/transcriptomics/salinispora/salinispora_tropica_cnb_440/fastq_files
# -working_dir
# /media/ecoli/2gb/transcriptomics/salinispora/salinispora_tropica_cnb_440/
# -genome
# /media/ecoli/2gb/transcriptomics/salinispora/salinispora_tropica_cnb_440/reference_genome/GCF_000016425.1_ASM1642v1_genomic.gbff
# -name
# salinispora_tropica_cnb440
# -cpu
# "10"
# -exp_design
# pe


########################## amy japonica all####################
# -working_dir
# "/media/ecoli/2gb/transcriptomics/amy_japonicum/zn24h_wt_vs_zurko/,/media/ecoli/2gb/transcriptomics/amy_japonicum/24_control_wt_vs_zur_ko,/media/ecoli/2gb/transcriptomics/amy_japonicum/24zurko_zn_vs_control/,/media/ecoli/2gb/transcriptomics/amy_japonicum/zn60_wt_vs_zurko"
# -fastq_folder_path
# "/media/ecoli/2gb/transcriptomics/amy_japonicum/zn24h_wt_vs_zurko/fastq_files,/media/ecoli/2gb/transcriptomics/amy_japonicum/24_control_wt_vs_zur_ko/fastq_files,/media/ecoli/2gb/transcriptomics/amy_japonicum/24zurko_zn_vs_control/fastq_files,/media/ecoli/2gb/transcriptomics/amy_japonicum/zn60_wt_vs_zurko/fastq_files"
# -genome
# GCF_000732925.1
# -name
# amy_jap
# -cpu
# "10"
# -exp_design
# s
# -user_cluster
# /media/ecoli/2gb/transcriptomics/amy_japonicum/edds_cluster_gene_ids
# -refset
# actinobacteria
# -refset_path
# /media/ecoli/1tb/arts_references_backup/
###################################################just one
# -fastq_folder_path
# /media/ecoli/2gb/transcriptomics/amy_japonicum/24h_fastq/fastq_files
# -working_dir
# /media/ecoli/2gb/transcriptomics/amy_japonicum/24h_fastq/
# -genome
# GCF_000732925.1
# -name
# amy_jap
# -cpu
# "10"
# -exp_design
# s



###################3 streptomyces coelicolor a32 ################
# -fastq_folder_path
# /media/ecoli/2gb/transcriptomics/sc_a2_working_dir/abrb_del_sco2/abrb/wt_mut_36/fastq_files
# -working_dir
# /media/ecoli/2gb/transcriptomics/sc_a2_working_dir/abrb_del_sco2/abrb/wt_mut_36/
# -genome
# /media/ecoli/2gb/transcriptomics/sc_a2_working_dir/abrb_del_sco2/abrb/wt_mut_36/reference_genome/GCF_000203835.1_ASM20383v1_genomic.gbff
# -name
# streptomyces_coelicolor_a32
# -cpu
# "10"
# -exp_design
# pe


###ctt vs r5solid isme ####
# -sra_input_file
# /media/ecoli/2gb/transcriptomics/sc_a2_working_dir/isme_coculture_xanthus/ctt_vs_r5_solid.txt
# -working_dir
# /media/ecoli/2gb/transcriptomics/sc_a2_working_dir/isme_coculture_xanthus/ctt_vs_r5solid
# -genome
# /media/ecoli/2gb/transcriptomics/sc_a2_working_dir/abrb_del_sco2/abrb/wt_mut_36/reference_genome/GCF_000203835.1_ASM20383v1_genomic.gbff
# -name
# streptomyces_coelicolor_a32
# -cpu
# "10"
# -exp_design
# s
# -ncbi_annotation
# True
# -read_source
# ena
##### ctt vs 350 #####
# -sra_input_file
# /media/ecoli/2gb/transcriptomics/sc_a2_working_dir/isme_coculture_xanthus/ctt_vs_350.txt
# -working_dir
# /media/ecoli/2gb/transcriptomics/sc_a2_working_dir/isme_coculture_xanthus/ctt_vs_350
# -genome
# /media/ecoli/2gb/transcriptomics/sc_a2_working_dir/abrb_del_sco2/abrb/wt_mut_36/reference_genome/GCF_000203835.1_ASM20383v1_genomic.gbff
# -name
# streptomyces_coelicolor_a32
# -cpu
# "10"
# -exp_design
# s
# -ncbi_annotation
# True
# -read_source
# ena