#  Copyright (C) 2021,2022 Mehmet Direnc Mungan
#  University of Tuebingen
#  Interfaculty Institute of Microbiology and Infection Medicine
#  Lab of Nadine Ziemert, Div. of Microbiology/Biotechnology
#  Funding by the German Centre for Infection Research (DZIF)
#  #
#  This file is part of SeMa-Trap
#  SeMa-Trap is free software. you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  #
#  #This program is distributed in the hope that it will be useful,
#   #but WITHOUT ANY WARRANTY; without even the implied warranty of
#   #MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   #GNU General Public License for more details.
#
#  License: You should have received a copy of the GNU General Public License v3 with SeMa-Trap
#  A copy of the GPLv3 can also be found at: <http://www.gnu.org/licenses/>.

import os
import pathlib
import subprocess
import logging
import platform

def download_gtf_fna_gbk_file(genome, working_dir):
    logging.info("downloading gtf_fna")
    gff_files = os.path.join(working_dir, "gff_gtf")
    if os.path.exists(gff_files) != True:
        os.mkdir(gff_files)
    reference_genomes = os.path.join(working_dir, "reference_genome")
    acc = ""
    if os.path.exists(genome):
        genome_file = open(genome, "r")
        for line in genome_file:
            if genome.endswith("fna") or genome.endswith("faa") or genome.endswith("fasta"):
                acc = line.split()[0].strip(">")
                break
            else:
                if line.startswith("            Assembly: "):
                    acc = line.split("Assembly: ")[1].strip()
                    break
    else:
        acc = genome
    logging.info(acc)
    system = platform.system()
    if acc != "":
        esearch_result = subprocess.run("esearch -db assembly -query %s | esummary | "
                                        "xtract -pattern DocumentSummary -element FtpPath_RefSeq" %(acc), shell=True,stdin=subprocess.PIPE, stdout=subprocess.PIPE).stdout
    else:
        if os.path.exists(genome):
            genome_file = open(genome, "r")
            for line in genome_file:
                if line.startswith("LOCUS"):
                    acc = line.split()[1].strip()
                    break
        esearch_result = subprocess.run("esearch -db assembly -query %s | esummary | "
                                        "xtract -pattern DocumentSummary -element FtpPath_RefSeq" % (acc), shell=True,
                                        stdin=subprocess.PIPE, stdout=subprocess.PIPE).stdout
    logging.info(acc)
    logging.info("esearch complete")
    logging.info(esearch_result.decode('UTF-8'))
    esearch_result = esearch_result.decode('UTF-8').rstrip()
    logging.info("esearch_result : " + esearch_result)
    gcf_name = esearch_result.split("/")[-1]
    logging.info("gcf_name  " + gcf_name)
    logging.debug("Downloading gtf and fna")
    gcf_filename = gcf_name.strip() + "_genomic.gtf.gz"
    fna_filename = gcf_name.strip() + "_genomic.fna.gz"
    gbff_filename = gcf_name.strip() + "_genomic.gbff.gz"
    protein_filename = gcf_name.strip() + "_protein.faa.gz"
    logging.info("gcf_filename : " + gcf_filename)
    try:
        wget_cmd = "wget -nc %s -P %s" %(esearch_result + "/" + gcf_filename, gff_files)
        wget_cmd_gbff = "wget -nc %s -P %s" % (esearch_result + "/" + gbff_filename, reference_genomes)
        wget_cmd_fna = "wget -nc %s -P %s" % (esearch_result + "/" + fna_filename, reference_genomes)
        wget_cmd_protein = "wget -nc %s -P %s" % (esearch_result + "/" + protein_filename, reference_genomes)
        logging.info(wget_cmd)
        subprocess.call([wget_cmd], shell=True)
        subprocess.call([wget_cmd_fna], shell=True)
        subprocess.call([wget_cmd_gbff], shell=True)
        subprocess.call([wget_cmd_protein], shell=True)
        logging.info("wget completed")
        logging.debug("unzipping")
        gzip_cmd = "gzip -k -f -d %s" % (os.path.join(gff_files, gcf_filename))
        gzip_cmd_fna = "gzip -k -f -d %s" % (os.path.join(reference_genomes, fna_filename))
        gzip_cmd_gbff = "gzip -k -f -d %s" % (os.path.join(reference_genomes, gbff_filename))
        gzip_cmd_protein = "gzip -k -f -d %s" % (os.path.join(reference_genomes, protein_filename))
        logging.info(gzip_cmd)
        subprocess.call([gzip_cmd], shell=True)
        subprocess.call([gzip_cmd_fna], shell=True)
        subprocess.call([gzip_cmd_gbff], shell=True)
        subprocess.call([gzip_cmd_protein], shell=True)
        unzipped_gff_path = os.path.join(gff_files, gcf_filename[:-3])
        unzipped_fna_path = os.path.join(reference_genomes, fna_filename[:-3])
        unzipped_gbff_path = os.path.join(reference_genomes, gbff_filename[:-3])
        unzipped_protein_path = os.path.join(reference_genomes, protein_filename[:-3])
        return unzipped_gff_path, unzipped_fna_path, unzipped_gbff_path, unzipped_protein_path
    except:
        logging.error("wget or gzip failed")


def data_organize(fastq_files_dir, paired_or_single, input_list, read_source):
    data_dir_dict = {}
    condition_directories = input_list
    for cond_dir in condition_directories:
        rep = 0
        cond_dir_path = os.path.join(fastq_files_dir, cond_dir)
        data_dir_dict[cond_dir] = {}
        if paired_or_single == "pe":
            fastq_list = os.listdir(cond_dir_path)
            fastq_list.sort()
            for fastq_file_num in range(0, len(fastq_list), 2):
                fastq_file = fastq_list[fastq_file_num]
                if "trimmed_" in fastq_file:
                    continue
                fastq_file_path = os.path.join(cond_dir_path, fastq_file)
                if read_source == "sra":
                    cur_sample = fastq_file.split("_")[0]
                else:
                    cur_sample = fastq_file.split(".fast")[0]
                paired = fastq_list[fastq_file_num:fastq_file_num+2]
                data_dir_dict[cond_dir][cur_sample] = [os.path.join(cond_dir_path, i) for i in paired]
        elif paired_or_single =="s":
            for fastq_file in os.listdir(cond_dir_path):
                if "trimmed_" in fastq_file:
                    continue
                fastq_file_path = os.path.join(cond_dir_path, fastq_file)
                if read_source == "sra":
                    cur_sample = fastq_file.split(".fast")[0]
                else:
                    cur_sample = fastq_file.split(".fast")[0]
                data_dir_dict[cond_dir][cur_sample] = [fastq_file_path]
        else:
            logging.error("unknown parameter %s" %paired_or_single)
            return
    return data_dir_dict

def trimming(fastp_path, paired_or_single, fastq_files_dir, input_list, read_source):
    data_dir_dict = data_organize(fastq_files_dir, paired_or_single, input_list, read_source)
    logging.info("trimming data dir dict " + str(data_dir_dict))
    for condition in data_dir_dict:
        for sample in data_dir_dict[condition]:
            if data_dir_dict[condition][sample][0].endswith(".fastq.gz"):
                gzip = ""
            else:
                gzip = ""
            if paired_or_single == "pe":
                print(data_dir_dict[condition][sample][0].replace(os.path.basename(data_dir_dict[condition][sample][0]),
                    "trimmed_" + os.path.basename(data_dir_dict[condition][sample][0])))
                if os.path.exists(data_dir_dict[condition][sample][0].replace(os.path.basename(data_dir_dict[condition][sample][0]),
                    "trimmed_" + os.path.basename(data_dir_dict[condition][sample][0]))) == False:
                    logging.info(fastp_path +" -i \"%s\" -I \"%s\" -o \"%s\" -O \"%s\" %s "%(data_dir_dict[condition][sample][0], data_dir_dict[condition][sample][1],
                    data_dir_dict[condition][sample][0].replace(os.path.basename(data_dir_dict[condition][sample][0]),
                    "trimmed_" + os.path.basename(data_dir_dict[condition][sample][0])),
                    data_dir_dict[condition][sample][1].replace(os.path.basename(data_dir_dict[condition][sample][1]),
                    "trimmed_" + os.path.basename(data_dir_dict[condition][sample][1])), gzip))
                    subprocess.call([fastp_path +" -i \"%s\" -I \"%s\" -o \"%s\" -O \"%s\" %s "%(data_dir_dict[condition][sample][0], data_dir_dict[condition][sample][1],
                    data_dir_dict[condition][sample][0].replace(os.path.basename(data_dir_dict[condition][sample][0]),
                    "trimmed_" + os.path.basename(data_dir_dict[condition][sample][0])),
                    data_dir_dict[condition][sample][1].replace(os.path.basename(data_dir_dict[condition][sample][1]),
                    "trimmed_" + os.path.basename(data_dir_dict[condition][sample][1])), gzip)], shell=True)
                data_dir_dict[condition][sample][0] = data_dir_dict[condition][sample][0].replace(os.path.basename(data_dir_dict[condition][sample][0]),
                "trimmed_" + os.path.basename(data_dir_dict[condition][sample][0]))
                data_dir_dict[condition][sample][1] = data_dir_dict[condition][sample][1].replace(os.path.basename(data_dir_dict[condition][sample][1]),
                "trimmed_" + os.path.basename(data_dir_dict[condition][sample][1]))
            else:
                if os.path.exists(data_dir_dict[condition][sample][0].replace(
                        os.path.basename(data_dir_dict[condition][sample][0]),
                        "trimmed_" + os.path.basename(data_dir_dict[condition][sample][0]))) == False:
                    logging.info(fastp_path + " -i \"%s\" -o \"%s\" %s " %(data_dir_dict[condition][sample][0],
                                    data_dir_dict[condition][sample][0].replace(
                        os.path.basename(data_dir_dict[condition][sample][0]),
                        "trimmed_" + os.path.basename(data_dir_dict[condition][sample][0])), gzip))
                    subprocess.call([fastp_path + " -i \"%s\" -o \"%s\" %s " %(data_dir_dict[condition][sample][0],
                                    data_dir_dict[condition][sample][0].replace(
                        os.path.basename(data_dir_dict[condition][sample][0]),
                        "trimmed_" + os.path.basename(data_dir_dict[condition][sample][0])), gzip)], shell=True)
                data_dir_dict[condition][sample][0] = data_dir_dict[condition][sample][0].replace(os.path.basename(data_dir_dict[condition][sample][0]),
                    "trimmed_" + os.path.basename(data_dir_dict[condition][sample][0]))
    return data_dir_dict


def hisat_build_index(reference_genome_path, index_output_path, genome_name):
    if os.path.exists(index_output_path) != True:
        os.mkdir(index_output_path)
    logging.info("hisat2-build \"%s\" \"%s\" -q" %(reference_genome_path, os.path.join(index_output_path, genome_name)))
    subprocess.call(["hisat2-build \"%s\" \"%s\" -q" %(reference_genome_path, os.path.join(index_output_path, genome_name))], shell=True)


def samtools_mapping(data_dir_dict, index_basename, out_dir, core_number, paired_or_single):
    if os.path.exists(out_dir) != True:
        os.mkdir(out_dir)
    if paired_or_single == "s":
        data_dir_dict_bams = {}
        for cond in data_dir_dict:
            data_dir_dict_bams[cond] = {}
            for sample in data_dir_dict[cond]:
                fastq_path = data_dir_dict[cond][sample][0]
                sorted_bam_out_file = os.path.join(out_dir, os.path.basename(fastq_path) + ".hisat.sorted.bam")
                if os.path.exists(sorted_bam_out_file):
                    print("samtools_mapping exists")
                    data_dir_dict_bams[cond][sample] = sorted_bam_out_file
                    continue
                logging.info("hisat2 -p 50 --no-spliced-alignment -x \"%s\" -U \"%s\" | samtools sort -n - -@ \"%s\" -o \"%s\""%(index_basename,fastq_path,str(core_number),sorted_bam_out_file))
                subprocess.call(["hisat2 -p 50 --no-spliced-alignment -x \"%s\" -U \"%s\" | samtools sort -n - -@ \"%s\" -o \"%s\""%(index_basename,fastq_path,str(core_number),sorted_bam_out_file)], shell=True)
                data_dir_dict_bams[cond][sample] = sorted_bam_out_file
    elif paired_or_single == "pe":
        data_dir_dict_bams = {}
        for cond in data_dir_dict:
            data_dir_dict_bams[cond] = {}
            for sample in data_dir_dict[cond]:
                for fastq in data_dir_dict[cond][sample]:
                    if fastq.endswith(".fastq.gz"):
                        gzip = ".gz"
                    else:
                        gzip = ""
                    if "_1.fastq" in os.path.basename(fastq):
                        file1 = fastq
                    if "_2.fastq" in os.path.basename(fastq):
                        file2 = fastq
                sorted_bam_out_file = os.path.join(out_dir, os.path.basename(file1).replace("_1.fastq%s"%gzip, "") + ".hisat.sorted.bam")
                if os.path.exists(sorted_bam_out_file):
                    print("samtools_mapping exists")
                    data_dir_dict_bams[cond][sample] = sorted_bam_out_file
                    continue

                logging.info("hisat2 -p 50 --no-spliced-alignment -x \"%s\" -1 \"%s\" -2 \"%s\" "
                             "| samtools sort -n - -@ \"%s\" -o \"%s\""%(index_basename,file1, file2, str(core_number), sorted_bam_out_file))
                subprocess.call(["hisat2 -p 50 --no-spliced-alignment -x \"%s\" -1 \"%s\" -2 \"%s\" "
                             "| samtools sort -n - -@ \"%s\" -o \"%s\""%(index_basename,file1, file2, str(core_number), sorted_bam_out_file)], shell=True)
                data_dir_dict_bams[cond][sample] = sorted_bam_out_file
    return data_dir_dict_bams




def convert_gff_to_gtf(old_gtf_filepath):
    new_filepath = old_gtf_filepath[0:-4] + "_new.gtf"
    new_gtf_file = open(new_filepath, "w")
    tr = 0
    with open(old_gtf_filepath,"r") as old_gtf_file:
        for line in old_gtf_file:
            if line.startswith("#"):
                new_gtf_file.write(line)
                continue
            else:
                if line.split("\t")[2] == "CDS":
                    tr += 1
                    new_gtf_file.write(line.replace("\tCDS\t",
                                                    "\ttranscript\t").replace("\t%s\t"%line.split("\t")[4],
                                                    "\t%s\t"%str(int(line.split("\t")[4]) + 3)).replace("unknown_transcript_1", "unknown_transcript_" + str(tr)))
                    new_gtf_file.write(line.replace("\tCDS\t",
                                                    "\texon\t").replace("\t%s\t"%line.split("\t")[4],
                                                    "\t%s\t"%str(int(line.split("\t")[4]) + 3)).replace("unknown_transcript_1", "unknown_transcript_" + str(tr)))
                    new_gtf_file.write(line.replace("unknown_transcript_1", "unknown_transcript_" + str(tr)))
                else:
                    new_gtf_file.write(line.replace("unknown_transcript_1", "unknown_transcript_" + str(tr)))
    new_gtf_file.close()
    return new_filepath





def run_qualimap(bam_dir_path,gtf_filepath, out_dir, ps):
    print("qualimap")
    logging.info("qualimap starting")
    if ps == "pe":
        ps = " -pe"
    elif ps == "s":
        ps = ""
    else:
        logging.error("Please pick paired or single")
    for file in os.listdir(bam_dir_path):
        if file.endswith(".bam") != True:
            continue
        else:
            qmap_out_dir = os.path.join(out_dir, os.path.basename(file))
            if os.path.exists(qmap_out_dir) != True:
                os.mkdir(qmap_out_dir)
            logging.info("qualimap rnaseq%s -s -bam %s -gtf %s -outdir %s " % (ps, os.path.join(bam_dir_path, file), gtf_filepath, qmap_out_dir))
            subprocess.call(["qualimap rnaseq%s -s -bam %s -gtf %s -outdir %s " % (ps, os.path.join(bam_dir_path, file), gtf_filepath, qmap_out_dir)], shell=True)
    logging.info("qualimap end")

def run_feature_count(cores, gtf_filepath, out_dir_path, bams, paired_or_single, master_dict, search_type):
    run = True
    logging.info("feature_count_start")
    if len(list(bams.keys())) == 0:
        logging.error("no bam file")
        return
    else:
        bam_files = ""
        for i in bams:
            for y in bams[i]:
                z = bams[i][y]
                bam_files = bam_files + z + " "
        bam_files = bam_files[0:-1]
    out_file = os.path.join(out_dir_path, "features.txt")
    if os.path.exists(out_file):
        print("feature count exists")
        run = False
    if run == True:
        logging.info("multi mapping reads also counted with -M option with primary tags and all reads instead of fragments are counted")
        if paired_or_single == "pe":
            logging.info("featureCounts -M -p -T %s -a %s -o %s --extraAttributes protein_id,product %s"
                         % (cores, gtf_filepath, out_file, bam_files))
            subprocess.call(["featureCounts -M -p -T %s -a %s -o %s --extraAttributes protein_id,product %s"
                             % (cores, gtf_filepath, out_file, bam_files)], shell=True)
        else:
            logging.info("featureCounts -M -T %s -a %s -o %s --extraAttributes protein_id,product %s" % (
            cores, gtf_filepath, out_file, bam_files))
            subprocess.call(["featureCounts -M -T %s -a %s -o %s --extraAttributes protein_id,product %s" % (
            cores, gtf_filepath, out_file, bam_files)], shell=True)
        logging.info("feature_count_end")
    print([os.path.join(out_dir_path, os.path.basename(i) + ".featureCounts.sam") for i in bams])
    print("featureCounts -T %s -a %s -o %s --extraAttributes protein_id,product %s" % (
            cores, gtf_filepath, out_file, bam_files))
    new_feats = open(out_file + "new", "w")
    with open(out_file, "r") as feats:
        for line in feats:
            if line.startswith("#"):
                new_feats.write(line)
            elif line.startswith("Geneid"):
                new_feats.write("id\t" + line)
            else:
                new_feats.write(master_dict[line.split("\t")[0]]["GeneID"] + "\t" + line)
    new_feats.close()
    with open(out_file, "r") as feats:
        total_1 = 0.0
        total_2 = 0.0
        for line in feats:
            if line.startswith("#") or line.startswith("Geneid"):
                continue
            tp_length = float(line.split("\t")[5])
            read_count = line.split("\t")
            reads = [i.strip("\n") for i in read_count[8:]]
            if search_type != "single":
                read_count_mean1 = 0.0
                for read in range(0, int(len(reads)/2)):
                    read_count_mean1 = read_count_mean1 + float(int(reads[read]))
                read_count_mean1 = read_count_mean1/float(int(len(reads)/2))
                read_count_mean2 = 0.0
                for read in range(0, int(len(reads)/2)):
                    read_count_mean2 = read_count_mean2 + float(int(reads[int(len(reads)/2) + read]))
                read_count_mean2 = read_count_mean2/float(int(len(reads)/2))
                total_2 += (read_count_mean2 / tp_length)
            else:
                read_count_mean1 = 0.0
                for read in range(0, int(len(reads))):
                    read_count_mean1 = read_count_mean1 + float(int(reads[read]))
                read_count_mean1 = read_count_mean1/float(int(len(reads)))
            total_1 += (read_count_mean1 / tp_length)
    with open(os.path.join(out_dir_path, "tpms.txt"), "w") as tpms:
        with open(out_file, "r") as feats:
            for line in feats:
                if line.startswith("#") or line.startswith("Geneid"):
                    continue
                gene = line.split("\t")[0].strip()
                tp_length = float(line.split("\t")[5])
                read_count = line.split("\t")
                reads = [i.strip("\n") for i in read_count[8:]]
                if search_type != "single":
                    read_count_mean1 = 0.0
                    for read in range(0, int(len(reads) / 2)):
                        read_count_mean1 = read_count_mean1 + float(int(reads[read]))
                    read_count_mean1 = read_count_mean1 / float(int(len(reads) / 2))

                    read_count_mean2 = 0.0
                    for read in range(0, int(len(reads) / 2)):
                        read_count_mean2 = read_count_mean2 + float(int(reads[int(len(reads) / 2) + read]))
                    read_count_mean2 = read_count_mean2 / float(int(len(reads) / 2))
                    tpm1 = 1000000.0*(read_count_mean1/ tp_length) / total_1
                    tpm2 = 1000000.0*(read_count_mean2/ tp_length) / total_2
                    tpms.write(gene + "\t" + str(tpm1) + "\t" + str(tpm2) + "\n")
                    master_dict[gene]["tpm1"] = str(tpm1)
                    master_dict[gene]["tpm2"] = str(tpm2)
                else:
                    read_count_mean1 = 0.0
                    for read in range(0, int(len(reads))):
                        read_count_mean1 = read_count_mean1 + float(int(reads[read]))
                    read_count_mean1 = read_count_mean1 / float(int(len(reads)))
                    tpm1 = 1000000.0*(read_count_mean1/ tp_length) / total_1
                    tpms.write(gene + "\t" + str(tpm1) + "\n")
                    master_dict[gene]["tpm1"] = str(tpm1)
    return out_file, bams



def remapping(core_number, new_sam_paths):
    sorted_primary_bams = []
    for new_sam_path in new_sam_paths:
        primary_sam = new_sam_path[:-4] + "_only_primary.sam"
        primary_bam = primary_sam[0:-4] + ".bam"
        sorted_primary_bam = primary_sam[0:-4] + "_sorted.bam"
        if os.path.exists(sorted_primary_bam):
            sorted_primary_bams.append(sorted_primary_bam)
            print("remapping exists")
            continue
        subprocess.call(["grep -v _Secondary %s > %s" % (new_sam_path, primary_sam)], shell=True)
        subprocess.call(["samtools view %s -b -@ %s -o %s" % (primary_sam, core_number, primary_bam)], shell=True)
        subprocess.call(["samtools sort %s -@ %s -o %s" % (primary_bam,core_number, sorted_primary_bam)], shell=True)
        sorted_primary_bams.append(sorted_primary_bam)
    print(sorted_primary_bams)
    return sorted_primary_bams


def check_bam_header(bam, gtf, working_dir):
    bam_name = os.path.basename(bam)
    new_bam_path = os.path.join(os.path.dirname(bam), "new_" + os.path.basename(bam))
    locuses = []
    for line in open(gtf, "r"):
        if line.startswith("#"):
            continue
        loc = line.split()[0].strip()
        if loc not in locuses:
            locuses.append(loc)
    logging.info("locuses:" + str(locuses))
    old_header_path = os.path.join(working_dir, "header")
    new_header_path = os.path.join(working_dir, "new_header")
    logging.info("samtools view -H %s > %s"%(bam,old_header_path))
    subprocess.call(["samtools view -H %s > %s"%(bam,old_header_path)], shell=True)
    with open(new_header_path, "w") as new_head_file:
        for line in open(old_header_path, "r"):
            if line.startswith("@SQ") and "SN:" in line:
                to_change = line.split()[1]
                for locus in locuses:
                    if locus in to_change:
                        line = line.replace(to_change, "SN:%s"%locus)
                        new_head_file.write(line)
            else:
                new_head_file.write(line)
    logging.info("samtools reheader %s %s > %s"%(new_header_path,bam,new_bam_path))
    subprocess.call(["samtools reheader %s %s > %s"%(new_header_path,bam,new_bam_path)], shell=True)
    return new_bam_path


def bedtools_coverage(gtf_file, sorted_primary_bam, outdir):
    bedtools_result_file = os.path.join(outdir, "coverages.txt")
    bedtools_result_file2_path = os.path.join(outdir, "coverages_tpm.txt")
    bedtools_result_file2 = open(os.path.join(outdir, "coverages_tpm.txt"), "w")
    subprocess.call(["bedtools coverage -a %s -b %s > %s"%(gtf_file, " ".join(sorted_primary_bam), bedtools_result_file)],  shell=True)
    total = 0.0
    for line in open(bedtools_result_file, "r"):
        tp_length = float(line.split("\t")[-2])
        read_count = float(line.split("\t")[-4])
        total += (read_count * 1000.0 / tp_length)
    for line in open(bedtools_result_file, "r"):
        if line.split("\t")[2] == "CDS" or line.split("\t")[2] == "stop_codon" or line.split("\t")[2] == "start_codon":
            continue
        tp_length = float(line.split("\t")[-2])
        read_count = float(line.split("\t")[-4])
        tpm = (((read_count * 1000.0) / tp_length) * 1000000) /  total
        line = line.strip("\n") + "\t" + str(tpm) + "\n"
        bedtools_result_file2.write(line)

    return bedtools_result_file2_path










def filter_features(intersection_proteins, features_path, working_dir, coverages_path):
    coverages_dict = {}
    cov_file = open(coverages_path, "r")
    for line in cov_file:
        if line.split("\t")[2] != "gene":
            continue
        gene = line.split("gene_id")[1].split(";")[0].replace("\"","").strip()
        coverage = line.split("\t")[-1].strip()
        coverages_dict[gene] = coverage
    x = 0
    filtered_feature_filepath = os.path.join(working_dir, "filtered_features.txt")
    filtered_feature_file = open(filtered_feature_filepath, "w")
    for line in open(features_path, "r"):
        x += 1
        if x <= 2:
            filtered_feature_file.write(line)
        elif line.split("\t")[-3] in intersection_proteins:
            filtered_feature_file.write(line.strip("\n") + "\t" + coverages_dict[line.split("\t")[0]] + "\n")
        else:
            continue


def run_deseq2(rscript_path,features_file, bams, working_dir, con1, con2):
    out_file = os.path.join(working_dir, "deseq_out.tsv")
    descript_path = os.path.join(working_dir, "deseq_analysis.R")
    descript = open(descript_path, "w")
    con = "c("
    z = 0
    for i in bams:
        for y in bams[i]:
            z += 1
    x = 0
    for i in bams:
        for y in bams[i]:
            x += 1
            con +="\"" + i + "\""
            if x != z:
                con += ", "
            else:
                con += ")"
    deseq_template_path = os.path.join(pathlib.Path(__file__).parent.parent.resolve(), "static/deseqstuff.R")
    with open(deseq_template_path, "r") as de:
        for line in de:
            if line.startswith("condition = \"tobechanged\""):
                descript.write(line.replace("\"tobechanged\"", con))
            elif line.startswith("columns = \"tobechanged\""):
                descript.write(line.replace("\"tobechanged\"", con))
            elif line.startswith("res "):
                descript.write(line.replace("\"tobechanged\"", "\"%s\",\"%s\""%(con2,con1)))
            else:
                descript.write(line)
    descript.close()
    logging.info(" ".join([rscript_path + " " + descript_path +" " + features_file + " " + out_file]))
    subprocess.call([rscript_path + " " + descript_path +" " + features_file + " " + out_file], shell=True)
    return out_file






if __name__ == '__main__':
    print("main")


