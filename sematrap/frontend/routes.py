#  Copyright (C) 2021,2022 Mehmet Direnc Mungan
#  University of Tuebingen
#  Interfaculty Institute of Microbiology and Infection Medicine
#  Lab of Nadine Ziemert, Div. of Microbiology/Biotechnology
#  Funding by the German Centre for Infection Research (DZIF)
#  #
#  This file is part of SeMa-Trap
#  SeMa-Trap is free software. you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  #
#  #This program is distributed in the hope that it will be useful,
#   #but WITHOUT ANY WARRANTY; without even the implied warranty of
#   #MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   #GNU General Public License for more details.
#
#  License: You should have received a copy of the GNU General Public License v3 with SeMa-Trap
#  A copy of the GPLv3 can also be found at: <http://www.gnu.org/licenses/>.

from flask import render_template, request, url_for, redirect, flash, session, Response, send_from_directory, abort
import redis, re, time, json, pickle
from rq import Queue, get_current_job
from sematrap import app
from werkzeug.utils import secure_filename
from sematrap.backend.pipeline import *
from sematrap.backend.download_fastq import check_read_ftp
from sematrap.frontend.routines import *
from rq.job import Job
from time import sleep


##TODO## take care of mail things##

@app.route('/', methods=['GET', 'POST'])
@app.route('/home', methods=['GET', 'POST'])
def home():
    return render_template("home.html", title ="Home")

@app.route('/', methods=['GET', 'POST'])
@app.route('/download', methods=['GET', 'POST'])
def download():
    return render_template("download.html", title ="Download")

@app.route('/about')
def about():
    return render_template("about.html", title ="About")

def check_file_upload(file):
    filename = secure_filename(file.filename)
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        return filename
    else:
        return False

def check_file_upload_bam(file):
    filename = secure_filename(file.filename)
    if file and allowed_file_bams(file.filename):
        filename = secure_filename(file.filename)
        return filename
    else:
        return False




@app.route('/analyze', methods=['GET', 'POST'])
# @app.route('/analyze')
def analyze():
    session.pop('_flashes', None)
    resistance = app.config['resistance']
    regulators = app.config['regulators']
    refset_folder = app.config["refset_folder"]
    rscript_path = app.config["rscript_path"]
    if request.method == 'POST':
        base_working_dir = unique_results_folder(app.config['results_folder'])
        os.mkdir(base_working_dir)
        base_job_id = os.path.basename(base_working_dir)
        form_dict = request.form.to_dict(flat=False)
        genome = form_dict["genome name"][0]
        if genome == "":
            genome = "Unknown_organism"
        genome = genome.replace(" ", "_")
        genome = (re.sub(r"[^a-zA-Z0-9]+", '_', genome))
        form_dict = convert_cond_names(form_dict)
        sra_dict = create_sra_input(form_dict)
        if len(sra_dict) > 2:
            multiple = True
        else:
            multiple = False
        print(multiple)
        print("sra_dict:", sra_dict)
        print("form_dict:", sra_dict)
        gbk_file = request.files['file']
        all_bam_files = [request.files.keys()]
        print(all_bam_files)
        acc = form_dict["ncbiacc1"][0]
        print("acc")
        search_type = form_dict["search_type"][0]
        print(search_type)
        initial_folder = os.path.join(app.config['uploads_folder'], str(uuid.uuid4()))
        if os.path.exists(initial_folder) != True:
            os.mkdir(initial_folder)
        defined_clusters = request.files['defined_clusters']
        defined_clusters_filename = secure_filename(defined_clusters.filename)
        if defined_clusters_filename != "":
            defined_clusters_path = os.path.join(initial_folder, defined_clusters_filename)
            if os.path.exists(defined_clusters_path):
                while os.path.exists(defined_clusters_path):
                    defined_clusters_path += "_a"
            defined_clusters.save(defined_clusters_path)
        else:
            defined_clusters_path = None
        if acc != "":
            esearch_result = check_ncbiacc(acc)
            if esearch_result == "":
                if os.path.exists(base_working_dir):
                    shutil.rmtree(base_working_dir)
                flash('Input accession not found, please check your input')
                return render_template("analyze.html", title ="Analyze")
            else:
                gbk_path = download_gbk_file(esearch_result, initial_folder)
                annotation = True

        else:
            filename = check_file_upload(gbk_file)
            if filename == False:
                if os.path.exists(base_working_dir):
                    shutil.rmtree(base_working_dir)
                flash('Filetype not allowed')
                return render_template("analyze.html", title ="Analyze")
            else:
                gbk_path = os.path.join(initial_folder, filename)
                if os.path.exists(gbk_path):
                    os.remove(gbk_path)
                gbk_file.save(gbk_path)
                esearch_result = check_ncbiacc(acc)
                if esearch_result != "":
                    annotation = True
                else:
                    annotation = False
        if search_type == "bam":
            bam_dict = {}
            for bam_key in request.files.keys():
                print(bam_key, request.files[bam_key], check_file_upload_bam(request.files[bam_key]))
                if bam_key == "file" or bam_key == "defined_clusters":
                    continue
                else:
                    file_name = check_file_upload_bam(request.files[bam_key])
                    if file_name == False:
                        flash('Filetype not allowed ' + bam_key)
                        return render_template("analyze.html", title ="Analyze")
                    else:
                        bam_dict[bam_key] = [file_name]
            sra_dict = create_sra_input_for_bam(form_dict, bam_dict)
            working_dir_sra_dict = create_sra_input_files(sra_dict, base_working_dir, genome)
            for bam_file in bam_dict:
                bam_to_save = bam_dict[bam_file][0]
                bam_path = os.path.join(app.config['uploads_folder'], initial_folder, bam_to_save)
                bam_file = request.files[bam_file]
                if os.path.exists(bam_path) != True:
                    bam_file.save(bam_path)
        elif search_type == "dual":
            sra_dict = create_sra_input(form_dict)
            working_dir_sra_dict = create_sra_input_files(sra_dict, base_working_dir, genome)
        elif search_type == "single":
            sra_dict = create_sra_input(form_dict)
            working_dir_sra_dict = create_sra_input_files_single(sra_dict, base_working_dir, genome)
        for i in form_dict:
            if i.startswith("replicate"):
                print("inside for i ")
                acc = form_dict[i][0].strip()
                sra_bool = True
                for tries in range(0, 3):
                    sra_bool = check_read_availability(base_working_dir, acc)
                    if sra_bool == False:
                        print("false " + acc)
                        continue
                    else:
                        break
                if os.path.exists(base_working_dir) and sra_bool == False:
                    shutil.rmtree(base_working_dir)
                    flash('Sra accession %s not found, please check your input'%acc)
                    return render_template("analyze.html", title ="Analyze")
        run_sema_trap(working_dir_sra_dict, form_dict, gbk_path, base_working_dir, regulators,
                        resistance, refset_folder, rscript_path, annotation, multiple, search_type, defined_clusters_path)
        return theresa_index(base_working_dir)
    return render_template("analyze.html", title ="Analyze")





def get_template(data, experiment, job_id, refresh=False):
    return render_template("show_results.html", result=data, refresh=refresh, experiment=experiment, job_id=job_id)


@app.route('/results/', defaults={"job_id":""}, methods=['GET','POST'])
@app.route('/results/<string:job_id>', methods=['GET','POST'])
def results(job_id):
    import datetime
    print(datetime.datetime.now().time())
    if job_id != "":
        return theresa_index(job_id)
    else:
        return render_template("results.html")


@app.route('/results/<jobid>/antismash_output')
@app.route('/results/<jobid>/antismash_output/')
def gotoantismashfiles(jobid):
    return redirect("/results/%s/antismash_output/index.html"%jobid)

@app.route('/results/<jobid>/antismash_output/<path:path>')
def getantismash(jobid,path):
    rd = os.path.join(app.config["results_folder"],jobid,"antismash_output")
    if os.path.exists(os.path.join(rd,path)):
        return send_from_directory(rd,path)
    return abort(404)


def show_results(data, refresh=False):
    return render_template("show_results.html", result=data, refresh=refresh)


def create_data(base_path):
    """Create data JSON"""
    data = {}
    comparisons = {}
    global_files_path = os.path.join(base_path, "global_files")
    conditions_path = os.path.join(base_path, "working_directory_conditions_dict.json")
    with open(conditions_path) as f:
        conditions = json.load(f)
    is_comp = not ('phantom_cond' in (o['con2'] for o in conditions.values()))
    for condition in conditions:
        conditions_path = os.path.join(base_path, condition, "experiment_specific_files")
        bgc_path = os.path.join(conditions_path, "all_bgcs_experimental.json")
        gene_path = os.path.join(conditions_path, "experiment_gene_dict.json")
        with open(bgc_path) as f:
            bgcs = json.load(f)
        with open(gene_path) as f:
            genes = json.load(f)
        if is_comp:
            comparisons[conditions[condition]['con2'] + '_vs_' + conditions[condition]['con1']] = {'bgcs': bgcs,
                                                                                                   'genes': genes,
                                                                                                   'conditions': list(
                                                                                                       conditions[
                                                                                                           condition].values())}
        else:
            comparisons[conditions[condition]['con1']] = {'bgcs': bgcs, 'genes': genes,
                                                          'conditions': [list(conditions[condition].values())[0]]}

    with open(os.path.join(global_files_path, 'kegg_primary_tree.json')) as f:
        kegg_primary_tree = json.load(f)
    with open(os.path.join(global_files_path, 'kegg_secondary_tree.json')) as f:
        kegg_secondary_tree = json.load(f)
    with open(os.path.join(global_files_path, 'kegg_primary_map.json')) as f:
        kegg_primary_map = json.load(f)
    with open(os.path.join(global_files_path, 'kegg_secondary_map.json')) as f:
        kegg_secondary_map = json.load(f)
    with open(os.path.join(global_files_path, 'all_bgc_genes_global.json')) as f:
        bgcs = json.load(f)
        BGCstarts = (o["bgc_start"] for o in bgcs.values())
        BGCnames = bgcs.keys()
        sortedNames = [x for _, x in sorted(zip(BGCstarts, BGCnames))]
        for BGC in bgcs:
            bgcs[BGC]["region_number"] = sortedNames.index(BGC) + 1
            bgcs[BGC]["name"] = BGC
    with open(os.path.join(global_files_path, 'annotation_gene_dict.json')) as f:
        annotations = json.load(f)
    with open(os.path.join(global_files_path, 'tpms.json')) as f:
        tpms = json.load(f)
    data['comparisons'] = comparisons
    data['bgcs'] = bgcs
    data['genes'] = annotations
    data['keggPrimary'] = {'hierarchy': kegg_primary_tree, 'geneMap': kegg_primary_map}
    data['keggSecondary'] = {'hierarchy': kegg_secondary_tree, 'geneMap': kegg_secondary_map}
    data['tpms'] = tpms
    data['isComp'] = is_comp
    return data

@app.route('/results', defaults={'data': None})
@app.route('/results/<data>')
def theresa_index(data):
    base_path = os.path.join(os.path.join(app.config['results_folder']), data)
    if os.path.isdir(base_path):
        return render_template('theresa_index.html', data=json.dumps(create_data(base_path)))
    else:
        abort(404)

@app.route('/help')
def help():
    return render_template("help.html")

# @app.route('/results/<job_id>')
# def results(job_id):
#     already_results = []
#     def get_status():
#
#         job = Job.fetch(job_id, connection=r)
#         status = job.get_status()
#
#         while status != 'finished':
#
#             status = job.get_status()
#             job.refresh()
#
#             d = {'status': status}
#
#             if 'progress' in job.meta:
#                 d['value'] = job.meta['progress']
#             else:
#                 d['value'] = "no progress"
#
#             # IF there's a result, add this to the stream
#             if job.result:
#                 d['result'] = job.result
#
#             json_data = json.dumps(d)
#             yield f"data:{json_data}\n\n"
#             time.sleep(1)
#
#     return Response(get_status(), mimetype='text/event-stream')


# status
# @app.route('/results/<jobid>/status')
# def jobstatus(jobid):
#     if jobid and routines.checkresult(jobid):
#         #rd = os.path.join(app.config["RESULTS_FOLDER"],jobid)
#         # statuscache = os.path.join(rd,"jobstatus.json")
#         # if not os.path.exists(statuscache) or os.path.getmtime(statuscache) < os.path.getmtime(os.path.join(rd,"arts-query.log")):
#         status = routines.getjobstatus(jobid)
#         return jsonify(status)
#         # else:
#         #     return send_from_directory(rd,"jobstatus.json")
#     return jsonify({"data":[]})