#  Copyright (C) 2021,2022 Mehmet Direnc Mungan
#  University of Tuebingen
#  Interfaculty Institute of Microbiology and Infection Medicine
#  Lab of Nadine Ziemert, Div. of Microbiology/Biotechnology
#  Funding by the German Centre for Infection Research (DZIF)
#  #
#  This file is part of SeMa-Trap
#  SeMa-Trap is free software. you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  #
#  #This program is distributed in the hope that it will be useful,
#   #but WITHOUT ANY WARRANTY; without even the implied warranty of
#   #MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   #GNU General Public License for more details.
#
#  License: You should have received a copy of the GNU General Public License v3 with SeMa-Trap
#  A copy of the GPLv3 can also be found at: <http://www.gnu.org/licenses/>.

import uuid, threading
from sematrap import app
from sematrap.backend.pipeline import *



def allowed_file(filename):
    ALLOWED_EXTENSIONS = {'fna', 'faa', 'gbk', 'gbff', 'fasta'}
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def allowed_file_bams(filename):
    ALLOWED_EXTENSIONS = {'bam'}
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS



def check_ncbiacc(acc):
    esearch_result = subprocess.run("esearch -db assembly -query %s | esummary | "
                                        "xtract -pattern DocumentSummary -element FtpPath_RefSeq" %(acc), shell=True,stdin=subprocess.PIPE, stdout=subprocess.PIPE).stdout
    esearch_result = esearch_result.decode('UTF-8').rstrip()
    return esearch_result

def download_gbk_file(esearch_res, uploads_folder):
    gcf_name = esearch_res.split("/")[-1]
    gbff_filename = gcf_name.strip() + "_genomic.gbff.gz"
    try:
        wget_cmd_gbff = "wget -nc %s -P %s" % (esearch_res + "/" + gbff_filename, uploads_folder)
        subprocess.call([wget_cmd_gbff], shell=True)
        ##TODO##
        ## change -k option ##
        gzip_cmd_gbff = "gzip -f -d %s" % (os.path.join(uploads_folder, gbff_filename))
        subprocess.call([gzip_cmd_gbff], shell=True)
        unzipped_gbff_path = os.path.join(uploads_folder, gbff_filename[:-3])
        return unzipped_gbff_path
    except:
        print("problem in acc")
        return

def run_sema_trap(working_dir_sra_dict, form_dict, gbk_path, base_working_dir, regulators,
                  resistance, refset_folder, rscript_path, annotation, multiple, search_type, user_cluster):
    refset = form_dict["refset"][0]
    read_source = "sra"
    genome = form_dict["genome name"][0]
    genome = genome.replace(" ", "_")
    genome = (re.sub(r"[^a-zA-Z0-9]+", '_', genome))
    library = form_dict["library layout"][0]
    egg_data_dir = app.config["eggnog_data_dir"]
    uploads_folder = app.config["uploads_folder"]
    cores = app.config["cores"]
    run_multiple(working_dir=",".join(list(working_dir_sra_dict.keys())), genome_name=genome,
                 cores=cores, paired_or_single=library, gbk_file=gbk_path, regulator_hmms=regulators,
                 sra_input=",".join(list(working_dir_sra_dict.values()))
                 , resistance_hmm_path=resistance,
                 rscript_path=rscript_path,
                 fastq_files=None,
                 ncbi_annotation=None,
                 read_source=read_source,
                 user_cluster=user_cluster,
                 refset=refset,
                 refset_path=refset_folder, annotation=annotation, multiple=multiple, egg_data_dir=egg_data_dir, search_type=search_type, uploads_folder=uploads_folder)
    # subprocess.call("source activate transcriptomics && /home/ubuntu/disk/transcriptomics/transcriptomics/backend/pipeline.py -working_dir %s "
    #                 "-genome_name %s -cores 50 -paired_or_single %s -gbk_file %s -regulator_hmms %s -sra_input %s -resistance_hmm_path %s -rscript_path %s -fastq_files"
    #                 " %s -ncbi_annotation %s -read_source %s -user_cluster %s -refset %s -refset_path %s"%(",".join(list(working_dir_sra_dict.keys())), genome,
    #                                                                                                        library, gbk_path, regulators, ",".join(list(working_dir_sra_dict.values())),
    #                                                                                                        resistance, rscript_path, None, None, read_source, None, refset, refset_folder), shell=True)


def checkresult(jobid):
    if os.path.isdir(os.path.join(app.config["RESULTS_FOLDER"],jobid)):
        return True
    else:
        return False



def unique_results_folder(base_folder):
    unique_filename = os.path.join(base_folder, str(uuid.uuid4()))
    return unique_filename

class check_sra(object):
    def __init__(self, cmd):
        self.cmd = cmd
        self.process = None

    def run2(self, timeout):
        def target():
            print("started")
            self.process = subprocess.Popen(self.cmd)
            self.process.communicate()
        thread = threading.Thread(target=target)
        thread.start()
        thread.join(timeout)
        if thread.is_alive():
            self.process.terminate()
            thread.join()
            return True
        else:
            return False

def convert_cond_names(form_dict):
    for i in form_dict:
        if i.startswith("condition_name"):
            cond = form_dict[i][0]
            import re
            cond = cond.strip()
            cond = cond.replace(" ", "_")
            cond = (re.sub(r"[^a-zA-Z0-9]+", '_', cond))
            form_dict[i] = [cond]
    return form_dict

def create_sra_input(form_dict):
    sra_dict = {}
    for i in form_dict:
        if i.startswith("condition_name"):
            tag = i.replace("condition_name", "")
            cond = form_dict[i][0].replace("\t", "")
            ## tag = 1_1, 1_2
            full_tag = tag + "|-|separator|-|" + cond
            if full_tag not in sra_dict:
                sra_dict[full_tag] = {form_dict[i][0]: []}
        if i.startswith("replicate"):
            tag = i.replace("replicate", "").split("_")[0] + "_" + i.replace("replicate", "").split("_")[1]
            for y in sra_dict:
                if tag == y.split("|-|separator|-|")[0]:
                    sra_dict[y][list(sra_dict[y].keys())[0]].append(form_dict[i][0])
    return sra_dict

def create_sra_input_for_bam(form_dict, bam_dict):
    sra_dict = {}
    for i in form_dict:
        if i.startswith("condition_name"):
            tag = i.replace("condition_name", "")
            cond = form_dict[i][0].replace("\t", "")
            ## tag = 1_1, 1_2
            full_tag = tag + "|-|separator|-|" + cond
            if full_tag not in sra_dict:
                sra_dict[full_tag] = {form_dict[i][0]: []}
    for i in bam_dict:
        if i.startswith("replicate"):
            tag = i.replace("replicate", "").split("_")[0] + "_" + i.replace("replicate", "").split("_")[1]
            for y in sra_dict:
                if tag == y.split("|-|separator|-|")[0]:
                    sra_dict[y][list(sra_dict[y].keys())[0]].append(bam_dict[i][0])
    return sra_dict


def create_sra_input_files(sra_dict, base_working_dir, genome):
    working_sra_dict = {}
    key_list = list(sra_dict.keys())
    for i in range(0, len(key_list), 2):
        key1 = key_list[i]
        key2 = key_list[i+1]
        ##TODO##
        ## change this ## double
        # return {"/home/ubuntu/disk/transcriptomics/transcriptomics/results/example/6f7ac68e-3488-4c61-9a1f-6ef9c481250e" :
        #            "/home/ubuntu/disk/transcriptomics/transcriptomics/results/example/6f7ac68e-3488-4c61-9a1f-6ef9c481250e/sra_input.txt",
        #        "/home/ubuntu/disk/transcriptomics/transcriptomics/results/example/7573f930-2416-498c-ac29-38d7c1c2a5b1" :
        # "/home/ubuntu/disk/transcriptomics/transcriptomics/results/example/7573f930-2416-498c-ac29-38d7c1c2a5b1/sra_input.txt"}
        ##single##
        # return {"/home/ubuntu/disk/transcriptomics/transcriptomics/results/532cb032-b845-4f39-9a27-4d5f47be6ca8/6cd39685-1372-4d93-a95c-294d6668bca5":
        #         "/home/ubuntu/disk/transcriptomics/transcriptomics/results/532cb032-b845-4f39-9a27-4d5f47be6ca8/6cd39685-1372-4d93-a95c-294d6668bca5/sra_input.txt"}
        working_dir = unique_results_folder(base_working_dir)
        os.mkdir(working_dir)
        #working_dir = "/home/ubuntu/disk/transcriptomics/test/d121a806-b30c-4fed-81f3-0d9c1d2b6328/27f3e003-7892-4658-9b41-f0e4e37eca1b"
        sra_path = os.path.join(working_dir, "sra_input.txt")
        file1 = open(sra_path, "w")
        condition1 = key1.split("|-|separator|-|")[1]
        condition2 = key2.split("|-|separator|-|")[1]
        sra_list1 = sra_dict[key1][condition1]
        sra_list2 = sra_dict[key2][condition2]
        for sra1 in sra_list1:
            file1.write(sra1 + "\t" + genome + "\t" + condition1 + "\n")
        for sra2 in sra_list2:
            file1.write(sra2 + "\t" + genome + "\t" + condition2 + "\n")
        file1.close()
        working_sra_dict[working_dir] = sra_path
    return working_sra_dict


def create_sra_input_files_single(sra_dict, base_working_dir, genome):
    working_sra_dict = {}
    key_list = list(sra_dict.keys())
    for i in range(0, len(key_list)):
        key1 = key_list[i]
        ##TODO##
        ## change this ## double
        # return {"/home/ubuntu/disk/transcriptomics/transcriptomics/results/example/6f7ac68e-3488-4c61-9a1f-6ef9c481250e" :
        #            "/home/ubuntu/disk/transcriptomics/transcriptomics/results/example/6f7ac68e-3488-4c61-9a1f-6ef9c481250e/sra_input.txt",
        #        "/home/ubuntu/disk/transcriptomics/transcriptomics/results/example/7573f930-2416-498c-ac29-38d7c1c2a5b1" :
        # "/home/ubuntu/disk/transcriptomics/transcriptomics/results/example/7573f930-2416-498c-ac29-38d7c1c2a5b1/sra_input.txt"}
        ##single##
        # return {"/home/ubuntu/disk/transcriptomics/transcriptomics/results/532cb032-b845-4f39-9a27-4d5f47be6ca8/6cd39685-1372-4d93-a95c-294d6668bca5":
        #         "/home/ubuntu/disk/transcriptomics/transcriptomics/results/532cb032-b845-4f39-9a27-4d5f47be6ca8/6cd39685-1372-4d93-a95c-294d6668bca5/sra_input.txt"}
        working_dir = unique_results_folder(base_working_dir)
        os.mkdir(working_dir)
        #working_dir = "/home/ubuntu/disk/transcriptomics/test/d121a806-b30c-4fed-81f3-0d9c1d2b6328/27f3e003-7892-4658-9b41-f0e4e37eca1b"
        sra_path = os.path.join(working_dir, "sra_input.txt")
        file1 = open(sra_path, "w")
        condition1 = key1.split("|-|separator|-|")[1]
        sra_list1 = sra_dict[key1][condition1]
        for sra1 in sra_list1:
            file1.write(sra1 + "\t" + genome + "\t" + condition1 + "\n")
        file1.close()
        working_sra_dict[working_dir] = sra_path
    return working_sra_dict


def generate_job_queue(redisid_to_jobid):
    queue_dict_pickle = os.path.join(app.config['static_folder'], "queue.pickle")
    if os.path.exists(queue_dict_pickle):
        with open(queue_dict_pickle, "rb") as qd:
            queue_dict = pickle.load(qd)
            for i in redisid_to_jobid:
                queue_dict[i] = redisid_to_jobid[i]
            with open(queue_dict_pickle, "wb") as qd:
                pickle.dump(queue_dict, qd)
    else:
        with open(queue_dict_pickle, "wb") as qd:
            pickle.dump(redisid_to_jobid, qd)


def add_finished_job(queue_dict, job_id):
    queue_dict_pickle = os.path.join(app.config['static_folder'], "queue.pickle")
    finished_job_dict_pickle = os.path.join(app.config['static_folder'], "finished_jobs.pickle")
    if os.path.exists(finished_job_dict_pickle):
        with open(finished_job_dict_pickle, "rb") as qd:
            finished_dict = pickle.load(qd)
            finished_dict[job_id] = queue_dict[job_id]
            with open(finished_job_dict_pickle, "wb") as qd:
                pickle.dump(finished_dict, qd)
    else:
        finished_dict = {job_id : queue_dict[job_id]}
        with open(finished_job_dict_pickle, "wb") as qd:
            pickle.dump(finished_dict, qd)



def check_job(job_id):
    finished_job_dict_pickle = os.path.join(app.config['static_folder'], "finished_jobs.pickle")
    if os.path.exists(finished_job_dict_pickle):
        with open(finished_job_dict_pickle, "rb") as qd:
            finished_dict = pickle.load(qd)
        if job_id in finished_dict:
            if os.path.exists(os.path.join(app.config['results_folder'], job_id, "antismash_output")) != True:
                del finished_dict[job_id]
                return False
            return finished_dict[job_id]
        else:
            return False
    else:
        return False

if __name__ == '__main__':
    form_dict = {'refset': ['actinobacteria'], 'read source': ['sra'], 'library layout': ['sra'], 'genome name': [''], 'ncbiacc1': [''], 'condition_name1_1': ['c1'], 'condition_name1_2': ['c2'], 'replicate1_1_1': ['1'], 'replicate1_2_2': ['2'], 'replicate1_1_3': ['3'], 'replicate1_2_4': ['4'], 'replicate1_1_6': ['5'], 'replicate1_2_6': ['6'], 'replicate1_1_7': ['7'], 'replicate1_2_7': ['8'], 'condition_name2_1': ['d1'], 'condition_name2_2': ['d2'], 'replicate2_1_5': ['9'], 'replicate2_2_5': ['10'], 'replicate2_1_6': ['11'], 'replicate2_2_6': ['12'], 'replicate2_1_9': ['15'], 'replicate2_2_9': ['16']}
    sra_dict = {'1_1|-|separator|-|c1': {'c1': ['1', '3', '5', '7']}, '1_2|-|separator|-|c2': {'c2': ['2', '4', '6', '8']}, '2_1|-|separator|-|d1': {'d1': ['9', '11', '15']}, '2_2|-|separator|-|d2': {'d2': ['10', '12', '16']}}
    genome = "asd"
    create_sra_input_files(sra_dict, "/media/ecoli/2gb/transcriptomics/test/047a538f-dac3-4a4f-a350-6789a91c7af7", genome)
    print("main")