
#install.packages('dplyr')
library(DESeq2)
library(dplyr)
args <- commandArgs(trailingOnly = TRUE)
countData = read.table(args[1],sep = '\t',
                       header =1, quote="", fill=FALSE)

countData <- select(countData, -Chr, -Start, -End, -Strand, -Length, -protein_id, -product)


#colnames(countData)[2:4] <- "wt_24"
rownames(countData) = countData$Geneid
countData = countData[,-1] #remove first column
# columns = c("wt_24_1", "wt_24_2", "wt_24_3", "mut_24_1", "mut_24_2", "mut_24_3")
columns = "tobechanged"
print(columns)
colnames(countData) = columns
# View(countData)
condition = "tobechanged"
# condition = c("control", "control", "control","mutant", "mutant", "mutant")
# condition <- eval(parse(text=args[3]))
colnames(countData)
colData = as.data.frame(cbind(colnames(countData), condition))
colData
dds <- DESeqDataSetFromMatrix(countData = countData,
                              colData = colData,
                              design = ~ condition)

dds <- DESeq(dds)
resultsNames(dds)
res <- results(dds, contrast=c("condition", "tobechanged"), cooksCutoff=FALSE, independentFiltering=FALSE)
head(res)
summary(res)
write.table(as.data.frame(res), file=args[2], sep="\t")
