#  Copyright (C) 2021,2022 Mehmet Direnc Mungan
#  University of Tuebingen
#  Interfaculty Institute of Microbiology and Infection Medicine
#  Lab of Nadine Ziemert, Div. of Microbiology/Biotechnology
#  Funding by the German Centre for Infection Research (DZIF)
#  #
#  This file is part of SeMa-Trap
#  SeMa-Trap is free software. you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  #
#  #This program is distributed in the hope that it will be useful,
#   #but WITHOUT ANY WARRANTY; without even the implied warranty of
#   #MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   #GNU General Public License for more details.
#
#  License: You should have received a copy of the GNU General Public License v3 with SeMa-Trap
#  A copy of the GPLv3 can also be found at: <http://www.gnu.org/licenses/>.

from flask import Flask
import os, pathlib, json

app = Flask(__name__)
app.secret_key = os.urandom(24)

path = os.path.dirname(os.path.abspath(__file__))

config_path = os.path.join(pathlib.Path(__file__).parent.resolve(), "static/configs.json")
with open(config_path) as conf:
    configs = json.load(conf)
# try:
for i in configs:
    if os.path.exists(configs[i]):
        app.config[i] = configs[i]
    else:
        print("Config file not set properly, please check your paths")
        break
# except:
app.config["static_folder"] = os.path.join(pathlib.Path(__file__).parent.resolve(), "static")
app.config["results_folder"] = os.path.join(pathlib.Path(__file__).parent.resolve(), "results")
app.config["uploads_folder"] = os.path.join(pathlib.Path(__file__).parent.resolve(), "uploads")
app.config["refset_folder"] = os.path.join(pathlib.Path(__file__).parent.resolve(), "reference_sets")
app.config["resistance"] = os.path.join(pathlib.Path(__file__).parent.resolve(), "static", "hmms", "knownresistance.hmm")
app.config["regulators"] = os.path.join(pathlib.Path(__file__).parent.resolve(), "static", "hmms", "all_regulators_pfam.hmm")
app.config["rscript_path"] = "Rscript"
app.config["fastq-dump"] = "fastq-dump-orig.2.11.1"
app.config["cores"] = "10"
app.config["eggnog_data_dir"] = os.path.join(pathlib.Path(__file__).parent.resolve(), "eggnog_data_dir")

print(configs)
print(config_path)




if os.path.exists(app.config["results_folder"]) != True:
    os.mkdir(app.config["results_folder"])

if os.path.exists(app.config["uploads_folder"]) != True:
    os.mkdir(app.config["uploads_folder"])

from sematrap.frontend import routes