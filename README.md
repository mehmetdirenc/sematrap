# README #

Local run instructions for SeMa-Trap.


Once you have installed the pipeline through conda 
(see "https://sema-trap.ziemertlab.com/download )
please change the paths in the "configs.json" file in "static" folder, e.g. "./sematrap/sematrap/static/configs.json"

Once the config file is set, you can run SeMa-Trap through terminal in the conda environment by typing:

"python /path/to/your_installation/sematrap/sematrap/frontend/app.py"

Afterwards, in terminal you will see a line starting with:

"Running on http://your ip:8000/"

Once you open that link through your web browser, you will be able to run SeMa-Trap like you would send a
job to the actual web-site.

